<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./libs/OwlCarousel2-2.3.4/owl.carousel.css">
    <link rel="stylesheet" href="./libs/OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" href="./libs/slick/slick.css">
    <link rel="stylesheet" href="./libs/slick/slick-theme.css">
    <link rel="stylesheet" href="./libs/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="./libs/animate/animate.css">
    <link rel="stylesheet" href="./libs/aos/aos.css">
    <link rel="stylesheet" href="./libs/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="./css/style.css">
    <title>Men Style</title>
</head>
<div class="main error">
    <a href="#" class="error__page">
        <div class="error__page__logo">
            <img src="./img/logo.png" alt="">
        </div>
        <div class="error__page-img">
            <img src="./img/404-2.png" alt="" class="error-des-img">
        </div>
    </a>
        <div class="info__bar">
            <div class="b-container">
                <div class="row justify-content-between align-items-end">
                    <ul class="col-md-3 social__list row align-items-center">
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon fb__icon"></i>
                                <span class="social__name">Facebook</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon inst__icon"></i>
                                <span class="social__name">Instagram</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="col-md-4 social__list row justify-content-end align-items-center">
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon phone__icon"></i>
                                <span class="social__name">+7 727 232 6265</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon pen__icon"></i>
                                <span class="social__name">Menstyle@gmail.ru</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#" class="go-to__main">
            <img src="./img/go-to-main.png" alt="">
        </a>
</div>