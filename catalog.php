<? include './includes/header2.php' ?>
<div class="main inner-padding">
    <section class="catalog__page">
        <div class="container">
            <div class="listing__controls__mobile">
                <button class="btn filter__open">Фильтр</button>
                <button class="btn btn__sort">Сортировка</button>
            </div>
            <div class="listing__filter-header row align-items-end">
                <div class="listing__filter_title col-md-3">
                    <div class="main__title">Брюки</div>
                </div>
                <div class="listing__controls col-md-9 row justify-content-between">
                    <div class="listing__controls--sort _desktop row">
                        <span class="listing__controls-title sorting_title">Сортировка по:</span>
                        <div class="close__btn"></div>
                        <ul class="row ">
                            <li><a href="#" class="sorting_title">Цене<i class="icon d-arrow__icon"></i></a></li>
                            <li><a href="#" class="sorting_title">Новинкам<i class="icon d-arrow__icon"></i></a></li>
                            <li><a href="#" class="sorting_title sale">Sale</a></li>
                        </ul>
                    </div>
                    <div class="listing__filter_right">
                        <span class="listing__filter_amount">1234 товаров</span>
                    </div>
                </div>
            </div>
            <div class="row">  
                <div class="col-md-3 filter__sidebar"> 
                    <div class="filter__header-mobile">
                        <div class="filter__title-mobile">Фильтр</div>
                        <a href="#" class="close__btn"></a>
                    </div>
                    <form class="filter__form">
                        <div class="filter__list">
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Одежда</div>
                                <div class="filter--item__content">
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Вся одежда</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-2">
                                        <label for="filter-2"></label>
                                        <span class="checkbox-text">Брюки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-3">
                                        <label for="filter-3"></label>
                                        <span class="checkbox-text">Пальто и куртки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-4">
                                        <label for="filter-4"></label>
                                        <span class="checkbox-text">Пуховики</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-5">
                                        <label for="filter-5"></label>
                                        <span class="checkbox-text">Жилеты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-6">
                                        <label for="filter-6"></label>
                                        <span class="checkbox-text">Костюмы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-7">
                                        <label for="filter-7"></label>
                                        <span class="checkbox-text">Деним</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-8">
                                        <label for="filter-8"></label>
                                        <span class="checkbox-text">Трикотаж</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-9">
                                        <label for="filter-9"></label>
                                        <span class="checkbox-text">Футболки поло</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-10">
                                        <label for="filter-10"></label>
                                        <span class="checkbox-text">Шорты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-11">
                                        <label for="filter-11"></label>
                                        <span class="checkbox-text">Пижамы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-12">
                                        <label for="filter-12"></label>
                                        <span class="checkbox-text">Майки</span>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Обувь</div>
                                <div class="filter--item__content">
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-13">
                                        <label for="filter-13"></label>
                                        <span class="checkbox-text">Вся одежда</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-14">
                                        <label for="filter-14"></label>
                                        <span class="checkbox-text">Брюки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-15">
                                        <label for="filter-15"></label>
                                        <span class="checkbox-text">Пальто и куртки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-16">
                                        <label for="filter-16"></label>
                                        <span class="checkbox-text">Пуховики</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-17">
                                        <label for="filter-17"></label>
                                        <span class="checkbox-text">Жилеты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-18">
                                        <label for="filter-18"></label>
                                        <span class="checkbox-text">Костюмы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-19">
                                        <label for="filter-19"></label>
                                        <span class="checkbox-text">Деним</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-20">
                                        <label for="filter-20"></label>
                                        <span class="checkbox-text">Трикотаж</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-21">
                                        <label for="filter-21"></label>
                                        <span class="checkbox-text">Футболки поло</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-22">
                                        <label for="filter-22"></label>
                                        <span class="checkbox-text">Шорты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-23">
                                        <label for="filter-23"></label>
                                        <span class="checkbox-text">Пижамы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-24">
                                        <label for="filter-24"></label>
                                        <span class="checkbox-text">Майки</span>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Аксессуары</div>
                                <div class="filter--item__content">
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-25">
                                        <label for="filter-25"></label>
                                        <span class="checkbox-text">Вся одежда</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-26">
                                        <label for="filter-26"></label>
                                        <span class="checkbox-text">Брюки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-27">
                                        <label for="filter-27"></label>
                                        <span class="checkbox-text">Пальто и куртки</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-28">
                                        <label for="filter-28"></label>
                                        <span class="checkbox-text">Пуховики</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-29">
                                        <label for="filter-29"></label>
                                        <span class="checkbox-text">Жилеты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-30">
                                        <label for="filter-30"></label>
                                        <span class="checkbox-text">Костюмы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-31">
                                        <label for="filter-31"></label>
                                        <span class="checkbox-text">Деним</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-32">
                                        <label for="filter-32"></label>
                                        <span class="checkbox-text">Трикотаж</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-33">
                                        <label for="filter-33"></label>
                                        <span class="checkbox-text">Футболки поло</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-34">
                                        <label for="filter-34"></label>
                                        <span class="checkbox-text">Шорты</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-35">
                                        <label for="filter-35"></label>
                                        <span class="checkbox-text">Пижамы</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-36">
                                        <label for="filter-36"></label>
                                        <span class="checkbox-text">Майки</span>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Цвет</div>
                                <div class="filter--item__content">
                                    <div class="filter-colors__list row">
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-1">
                                            <label for="filter-color-1" style="background-color: #e3e3e3;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-2">
                                            <label for="filter-color-2" style="background-color: #d2c0a8;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-3">
                                            <label for="filter-color-3" style="background-color: #191919;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-4">
                                            <label for="filter-color-4" style="background-color: #a9b1a6;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-5">
                                            <label for="filter-color-5" style="background-color: #c3975f;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-6">
                                            <label for="filter-color-6" style="background-color: #7b3a3a;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-7">
                                            <label for="filter-color-7" style="background-color: #2b498f;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-8">
                                            <label for="filter-color-8" style="background-color: #ff616f;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-9">
                                            <label for="filter-color-9" style="background-color: #743d03;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-10">
                                            <label for="filter-color-10" style="background-color: #ffeb1b;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-11">
                                            <label for="filter-color-11" style="background-color: #8bcdff;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-12">
                                            <label for="filter-color-12" style="background-color: #4879e5;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check" name="user_checker" id="filter-color-13">
                                            <label for="filter-color-13" style="background-color: #1eded4;"></label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input color-check _white" name="user_checker" id="filter-color-14">
                                            <label for="filter-color-14" style="background-color: #fff;"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Размер</div>
                                <div class="filter--item__content">
                                    <div class="filter-size__list row">
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input size-check" name="user_checker" id="filter-size-1">
                                            <label for="filter-size-1">42</label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input size-check" name="user_checker" id="filter-size-2">
                                            <label for="filter-size-2">44</label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input size-check" name="user_checker" id="filter-size-3">
                                            <label for="filter-size-3">46</label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input size-check" name="user_checker" id="filter-size-4">
                                            <label for="filter-size-4">48</label>
                                        </div>
                                        <div class="checking">
                                            <input type="checkbox" class="checkbox-input size-check" name="user_checker" id="filter-size-5">
                                            <label for="filter-size-5">50</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Цена</div>
                                <div class="filter--item__content">
                                    <form class="range-form">
                                        <div class="filter-range-block">
                                            <div class="row justify-content-between range-slider__inputs">
                                                <div class="range-slider__input">
                                                    <input type="text" class="input input_range min" onfocus="this.placeholder=''" placeholder="0">
                                                </div>
                                                <div class="range-slider__input">
                                                    <input type="text" class="input input_range max" onfocus="this.placeholder=''" placeholder="2 000 000">
                                                </div>
                                            </div>
                                            <div class="range slider-range price-filter-range" name="rangeInput" data-min="0" data-max="2000000"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Бренд</div>
                                <div class="filter--item__content">
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Baumler</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Benvenuto</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Bomboogie</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Brax</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Casamoda</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Claudio Compione</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Dalmine</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Daniel Hechter</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Falke</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Fraas</span>
                                    </div>
                                </div>
                            </div>
                            <div class="filter--item">
                                <div class="filter--item-title down__toggle">Сезон</div>
                                <div class="filter--item__content">
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Baumler</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Benvenuto</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Bomboogie</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Brax</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Casamoda</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Claudio Compione</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Dalmine</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Daniel Hechter</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Falke</span>
                                    </div>
                                    <div class="checking">
                                        <input type="checkbox" class="checkbox-input" name="user_checker" id="filter-1">
                                        <label for="filter-1"></label>
                                        <span class="checkbox-text">Fraas</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn__fill_none btn__orange btn__filter" type="button">Сбросить фильтр</button>
                        <div class="filter__btn-mobile justify-content-between">
                            <button class="btn btn__fill_none btn__border__black" type="button">Сбросить</button>
                            <button class="btn btn__fill__black" type="button">Применить</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-9">               
                    <div class="product__list filter__products row">
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-1.jpg">
                                <div class="sticker">
                                    <span>NEW</span>
                                </div>
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-1.jpg">
                                <div class="sticker">
                                    <span>NEW</span>
                                </div>
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-2.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">55.000 <span class="valute">₸</span> <span class="old__price">75.000 ₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-3.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-4.jpeg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-2.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-2.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-2.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                        <a href="#" class="product__block">
                            <div class="product__img">
                                <img src="./img/product-2.jpg">
                                <div class="product__hover">Купить</div>
                            </div>
                            <div class="product__bottom">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__price">75.000 <span class="valute">₸</span></div>
                            </div>
                        </a>
                    </div>
                    <div class="pagination">
                        <a href="#" class="prev nonactive"><i class="icon arrow__icon"></i> <span>Prev</span> </a>
                        <ul>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li>-</li>
                            <li><a href="#">120</a></li>
                        </ul>
                        <a href="#" class="next"><span>Next</span> <i class="icon arrow__icon"></i></a>
                    </div>
                    <div class="catalog__description row justify-content-between">
                        <div class="col-md-3 desc__name">
                            <span>Брюки</span>
                        </div>
                        <div class="col-md-9 desc__text">
                            <p>Джинсы — не единственный вариант на каждый день. Мы предлагаем модели брюк в строгом или непринужденном стиле, как для офиса, так и вне его. Брюки чинос отлично подойдут для встречи с друзьями в пятницу после работы. В шортах можно отправиться на прогулку, а брюки из вельвета пригодятся в выходные, когда на улице холодно. Брюки-джоггеры — идеальный выбор для расслабленного отдыха, а классические брюки с защипами разных цветов и принтов подходят для любого случая. Мы расширили свой ассортимент брюк и обновили фасоны в соответствии с ритмом жизни современного мужчины.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>