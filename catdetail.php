<? include './includes/header.php' ?>
<div class="main bg-gray p-95">
    <section id="s-breadcrumbs">
        <div class="container">
            <div class="breadcrumbs">
                <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list">
                    <li itemprop="itemListElement" itemscope
                    itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="#" class="breadcrumb__link">
                        <span itemprop="name">Главная</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope
                    itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="#" class="breadcrumb__link">
                        <span itemprop="name">Одежда</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope
                    itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="#" class="breadcrumb__link">
                        <span itemprop="name">Брюки</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope
                    itemtype="http://schema.org/ListItem">
                        <span itemprop="item"class="breadcrumb__link current">
                        <span itemprop="name"> Bugatti COAT - Classic coat - navy</span></span>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
            <a href="#" class="back__btn"><i class="icon arrow__icon"></i>Вернуться назад</a>
        </div>
    </section>
    <section class="cat-deatil">
        <div class="container">
            <div class="row justify-content-between cat__detail-content">
                <div class="col-md-6">
                    <div class="cat__detail-nav">
                        <div class="slick-item">
                            <img src="./img/cat-1.png">
                        </div>
                        <div class="slick-item">
                            <img src="./img/cat-2.png">
                        </div>
                        <div class="slick-item">
                            <img src="./img/cat-3.png">
                        </div>
                    </div>
                    <div class="cat__detail-for">
                        <div class="slick-item jqueryZoom">
                            <div class="card__image" style="background-image: url('./img/cat-1.png');"></div>
                            <img src="./img/zoom-img.png" class="zoomImg" style="display: none;">
                            <div class="sticker">
                                <span>NEW</span>
                            </div>
                        </div>
                        <div class="slick-item">
                            <div class="card__image" style="background-image: url('./img/cat-2.png');"></div>
                        </div>
                        <div class="slick-item">
                            <div class="card__image" style="background-image: url('./img/cat-3.png');"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 cat__detail-right">
                    <div class="cat__detail-top">
                        <span class="cat__code">Арт. PO006EMHTRD7</span>
                        <div class="cat__name">Bugatti COAT - Classic coat - navy</div>
                        <span class="cat-new__price">125. 999 <span>₸</span></span> 
                        <span class="cat-old__price">155.000</span>
                    </div>
                    <div class="cat__detail-char">
                        <ul class="tab-menu cat__char-tabs">
                            <li class="active"><a href="#detail-tab1">Информация</a></li>
                            <li><a href="#detail-tab2">описание</a></li>
                        </ul>
                        <div class="tab-item active" data-id="detail-tab1">
                            <div class="cat__info-item">
                                <span class="char__type">Бренд:</span>
                                <span class="char__value">Bugatti</span>
                            </div>
                            <div class="cat__info-item">
                                <span class="char__type">Категория:</span>
                                <span class="char__value">Брюки</span>
                            </div>
                            <div class="cat__info-item row align-items-center">
                                <span class="char__type">Цвет:</span>
                                <div class="colors__check-list row">
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input color-check" name="user_checker" id="filter-color-1">
                                        <label for="filter-color-1" style="background-color: #e3e3e3;"></label>
                                    </div>
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input color-check" name="user_checker" id="filter-color-2">
                                        <label for="filter-color-2" style="background-color: #d2c0a8;"></label>
                                    </div>
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input color-check" name="user_checker" id="filter-color-3">
                                        <label for="filter-color-3" style="background-color: #191919;"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="cat__info-item cat__info-item-size row align-items-center">
                                <span class="char__type">Размер:</span>
                                <div class="filter-size__list row">
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input size-check" name="user_checker" id="filter-size-1">
                                        <label for="filter-size-1">42</label>
                                    </div>
                                    <div class="checking chosen">
                                        <input type="radio" class="checkbox-input size-check" name="user_checker" id="filter-size-2">
                                        <label for="filter-size-2">44</label>
                                    </div>
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input size-check" name="user_checker" id="filter-size-3">
                                        <label for="filter-size-3">46</label>
                                    </div>
                                    <div class="checking">
                                        <input type="radio" class="checkbox-input size-check" name="user_checker" id="filter-size-4">
                                        <label for="filter-size-4">48</label>
                                    </div>
                                    <div class="checking absent">
                                        <input type="radio" class="checkbox-input size-check" name="user_checker" id="filter-size-5">
                                        <label for="filter-size-5">50</label>
                                    </div>
                                </div>
                                <a href="#" class="table__size open-modal-click" data-modal="#modal--size">Таблица размеров</a>
                            </div>
                        </div>
                        <div class="tab-item" data-id="detail-tab2">
                            <div class="cat__info-item">
                                <span class="char__type">Описание:</span>
                                <span class="char__value">Линия Casual. Slim fit. Хлопковый трикотаж стрейч. Саржа. Вощеный 
                                    деним. По бокам два кармана. Карман для монет. Сзади два накладных кармана. 
                                    Шлевки. Застежка на молнию и пуговицу.
                                    Длина шагового шва 83.0 cm.
                                    Боковая длина (от проймы до низа) 104.5 cm.
                                </span>
                            </div>
                            <div class="cat__info-item">
                                <span class="char__type">Состав и рекомендации по уходу:</span>
                                <span class="char__value">Состав: 97% хлопок,3% эластан. Подкладка кармана: 65% полиэстер,35% хлопок
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="cat__detail-bottom row justify-content-between align-items-center">
                        <div class="p-number">
                            <span class="minus disable">-</span>
                            <input type="text" value="1" class="num-input"/>
                            <span class="plus">+</span>
                        </div>
                        <button class="btn btn__fill__black add__cart open-modal-click" data-modal="#modal--cart">Добавить в корзину</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>
<? include './includes/modal.php' ?>