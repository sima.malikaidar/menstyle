<? include './includes/header.php' ?>
<div class="main bg-gray">
    <section class="contacts-page">
        <div class="container">
            <div class="row">
                <div class="col-md-7 contacts__left">
                    <div class="tab-item" data-id="address-tab1">
                        <div class="address__name">
                            <div class="address__city">г. Алматы</div>
                            <div class="address__st">Ул.Казыбек Би 12/24</div>
                        </div>
                        <div class="address__contacts row">
                            <div class="phone">
                                <i class="icon phone__icon"></i>
                            </div>
                            <ul class="phone--list">
                                <li>тел: <a href="#">+7 727 232 6265</a></li>
                                <li>тел: <a href="#">+7 727 232 6295</a></li>
                                <li>понедельник-суббота: <span>10.00 - 20.00</span></li>
                                <li>воскресенье: <span>10.00 - 19.00</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item active" data-id="address-tab2">
                        <div class="address__name">
                            <div class="address__city">г. Алматы</div>
                            <div class="address__st">Ул.Казыбек Би 12/24</div>
                        </div>
                        <div class="address__contacts row">
                            <div class="phone">
                                <i class="icon phone__icon"></i>
                            </div>
                            <ul class="phone--list">
                                <li>тел: <a href="#">+7 727 232 6265</a></li>
                                <li>тел: <a href="#">+7 727 232 6295</a></li>
                                <li>понедельник-суббота: <span>10.00 - 20.00</span></li>
                                <li>воскресенье: <span>10.00 - 19.00</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="address-tab3">
                        <div class="address__name">
                            <div class="address__city">г. Алматы</div>
                            <div class="address__st">Ул.Казыбек Би 12/24</div>
                        </div>
                        <div class="address__contacts row">
                            <div class="phone">
                                <i class="icon phone__icon"></i>
                            </div>
                            <ul class="phone--list">
                                <li>тел: <a href="#">+7 727 232 6265</a></li>
                                <li>тел: <a href="#">+7 727 232 6295</a></li>
                                <li>понедельник-суббота: <span>10.00 - 20.00</span></li>
                                <li>воскресенье: <span>10.00 - 19.00</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="address-tab4">
                        <div class="address__name">
                            <div class="address__city">г. Алматы</div>
                            <div class="address__st">Ул.Казыбек Би 12/24</div>
                        </div>
                        <div class="address__contacts row">
                            <div class="phone">
                                <i class="icon phone__icon"></i>
                            </div>
                            <ul class="phone--list">
                                <li>тел: <a href="#">+7 727 232 6265</a></li>
                                <li>тел: <a href="#">+7 727 232 6295</a></li>
                                <li>понедельник-суббота: <span>10.00 - 20.00</span></li>
                                <li>воскресенье: <span>10.00 - 19.00</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="address-tab5">
                        <div class="address__name">
                            <div class="address__city">г. Алматы</div>
                            <div class="address__st">Ул.Казыбек Би 12/24</div>
                        </div>
                        <div class="address__contacts row">
                            <div class="phone">
                                <i class="icon phone__icon"></i>
                            </div>
                            <ul class="phone--list">
                                <li>тел: <a href="#">+7 727 232 6265</a></li>
                                <li>тел: <a href="#">+7 727 232 6295</a></li>
                                <li>понедельник-суббота: <a href="#">10.00 - 20.00</a></li>
                                <li>воскресенье: <a href="#">10.00 - 19.00</a></li>
                            </ul>
                        </div>
                    </div>
                    <ul class="tab-menu city__tabs-menu row justify-content-between">
                        <li><a href="#address-tab1" data-wid="51.128207" data-len="71.430411">Нурсултан</a></li>
                        <li class="active"><a href="#address-tab2" data-wid="43.26832607452854" data-len="76.93681450000001">Алматы</a></li>
                        <li><a href="#address-tab3" data-wid="50.300371" data-len="57.154555">Актобе</a></li>
                        <li><a href="#address-tab4" data-wid="49.807754" data-len="73.088504">Караганда</a></li>
                        <li><a href="#address-tab5" data-wid="52.285577" data-len="76.940947">Павлодар</a></li>
                    </ul>
                    <div class="info__bar">
                        <div class="row justify-content-between align-items-end">
                            <ul class="col-md-5 social__list row align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                        <span class="social__name">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                        <span class="social__name">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="col-md-4 social__list row justify-content-end align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Menstyle@gmail.com</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="tab-menu city__tabs-menu _mobile owl-carousel">
                        <li><a href="#address-tab1" data-wid="43.416541" data-len="77.018213">Нурсултан</a></li>
                        <li class="active"><a href="#address-tab2" data-wid="43.215630" data-len="76.865084">Алматы</a></li>
                        <li><a href="#address-tab3" data-wid="50.300371" data-len="57.154555">Актобе</a></li>
                        <li><a href="#address-tab4" data-wid="49.807754" data-len="73.088504">Караганда</a></li>
                        <li><a href="#address-tab5" data-wid="52.285577" data-len="76.940947">Павлодар</a></li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <div id="map" data-id="c-map"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>