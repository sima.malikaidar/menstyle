<? include './includes/header.php' ?>
<div class="main p-90">
    <section class="checkout">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 checkout__left">
                    <a href="#" class="back__btn"><i class="icon arrow__icon"></i></a>
                    <span class="page__title2">Оформление заказа</span>
                    <form class="form-group">
                        <div class="sub__title">Личные данные</div>
                        <div class="row justify-content-between one-line">
                            <div class="input-text">
                                <input type="text" class="input" placeholder="Имя*" name="user_name">
                            </div>
                            <div class="input-text">
                                <input type="text" class="input" placeholder="Фамилия*" name="user_surname">
                            </div>
                        </div>
                        <div class="input-text">
                            <input type="text" class="input input_tel" placeholder="Номер телефона*" name="user_phone">
                            <i class="check__icon"></i>
                            <span class="tel__text">Ваш номер активирован в нашей базе и Вы получается дисконт в размере 10% скидки.</span>
                        </div>
                        <div class="input-text">
                            <input type="email" class="input" placeholder="E-mail*" name="user_email">
                        </div>
                        <div class="input-check">
                            <input type="checkbox" class="subs-check" id="subs-check" name="user_check">
                            <label for="subs-check"></label>
                            <span>Подписаться на новости и скидки </span>
                        </div>
                        <div class="input-text">
                            <input type="text" class="input" placeholder="Казахстан*" name="user_city" value="Казахстан" readonly>
                        </div>
                        <div class="row justify-content-between one-line">
                            <div class="input-text">
                                <select name="user__address" class="input select-ui">
                                    <option selected disabled hidden>Город</option>
                                    <option value="1">Нур-Султан</option>
                                    <option value="2">Алматы</option>
                                    <option value="3">Актобе</option>
                                </select>
                            </div>
                            <div class="input-text">
                                <input type="text" class="input" placeholder="Почтовый индекс*" name="user_index">
                            </div>
                        </div>
                        <div class="input-text">
                            <input type="text" class="input" placeholder="Точный адрес доставки*" name="user_delivery">
                        </div>
                        <div class="sub__title2">Способ доставки</div>
                        <div class="row">
                            <div class="input-radio">
                                <input type="radio" class="subs-radio" id="subs-radio-1" name="user__radio">
                                <label for="subs-radio-1"></label>
                                <span class="radio-text">Самовывоз</span>
                            </div>
                            <div class="input-radio">
                                <input type="radio" class="subs-radio" id="subs-radio-2" name="user__radio">
                                <label for="subs-radio-2"></label>
                                <span class="radio-text">Курьером</span>
                            </div>
                        </div>
                        <div class="checkout__info">
                            <p>Ожидаемая дата доставки Вторник, 30 июль, 2019
                                Доставка не осуществляется в установленные законом нерабочие дни. Заказы подлежат обложению таможенными пошлинами и сборами, которые оплачивает получатель.</p>
                            <a href="#" class="btn__more">Подробнее</a>
                        </div>
                        <div class="sub__title2">Способ оплаты</div>
                        <div class="row">
                            <div class="input-radio">
                                <input type="radio" class="subs-radio" id="subs-radio-3" name="user__radio-2">
                                <label for="subs-radio-3"></label>
                                <span class="radio-text">Наличными курьеру</span>
                            </div>
                            <div class="input-radio">
                                <input type="radio" class="subs-radio" id="subs-radio-4" name="user__radio-2">
                                <label for="subs-radio-4"></label>
                                <span class="radio-text">Оплата картой</span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-5 checkout__right">
                    <div class="breadcrumbs">
                        <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list">
                            <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="#" class="breadcrumb__link">
                                <span itemprop="name">Главная</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="#" class="breadcrumb__link">
                                <span itemprop="name">Корзина</span></a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <span itemprop="item"class="breadcrumb__link current">
                                <span itemprop="name"> Оформление заказа</span></span>
                                <meta itemprop="position" content="2" />
                            </li>
                        </ul>
                    </div>
                    <div class="checkout__content">
                        <a href="#" class="row align-items-center checkout__product">
                            <div class="cart__img">
                                <img src="./img/cart-1.png">
                            </div>
                            <div class="cart__info">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__detail">
                                    <span class="product__d-type">Размер:</span><span class="value">36</span>
                                </div>
                                <div class="product__detail">
                                    <span class="product__d-type">Количество:</span><span class="value">1</span>
                                </div>
                            </div>
                            <div class="product__price">30 500<span class="valute">тг</span></div>
                        </a>
                        <a href="#" class="row align-items-center checkout__product">
                            <div class="cart__img">
                                <img src="./img/cart-2.png">
                            </div>
                            <div class="cart__info">
                                <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="product__detail">
                                    <span class="product__d-type">Размер:</span><span class="value">36</span>
                                </div>
                                <div class="product__detail">
                                    <span class="product__d-type">Количество:</span><span class="value">1</span>
                                </div>
                            </div>
                            <div class="product__price">30 500<span class="valute">тг</span></div>
                        </a>
                        <form class="form-group row">
                            <div class="input-text">
                                <input type="text" class="input" placeholder="Введите промо код">
                            </div>
                            <button class="btn btn__fill__gray">Применить</button>
                        </form>
                        <ul class="price-table">
                            <li class="row justify-content-between">
                                <span>Товары на сумму</span>
                                <span>30 000 тг</span>
                            </li>
                            <li class="row discount justify-content-between">
                                <span>Скидка по промо коду</span>
                                <span>-7 000 тг</span>
                            </li>
                            <li class="row discount justify-content-between">
                                <span>Скидка на товар 10%</span>
                                <span>-1 000 тг</span>
                            </li>
                        </ul>
                        <div class="row justify-content-between total__price">
                            <span>Всего</span>
                            <span>23 000 тг</span>
                        </div>
                        <button class="btn btn__fill__black btn__buy">Перейти к оплате</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>