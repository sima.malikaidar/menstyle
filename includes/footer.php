    <footer class="footer">
        <div class="container">
            <div class="footer-top">
                <div class="f__container row justify-content-between">
                    <div class="col-md-3">
                        <div class="footer__title">Категории</div>
                        <ul>
                            <li><a href="#">Одежда</a></li>
                            <li><a href="#">Обувь</a></li>
                            <li><a href="#">Аксессуары</a></li>
                            <li><a href="#">Сумки</a></li>
                            <li><a href="#">Бренды</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="footer__title">информация</div>
                        <ul>
                            <li><a href="#">Одежда</a></li>
                            <li><a href="#">Обувь</a></li>
                            <li><a href="#">Аксессуары</a></li>
                            <li><a href="#">Сумки</a></li>
                            <li><a href="#">Бренды</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="footer__title">Магазины по городам</div>
                        <ul>
                            <li><a href="#">Одежда</a></li>
                            <li><a href="#">Обувь</a></li>
                            <li><a href="#">Аксессуары</a></li>
                            <li><a href="#">Сумки</a></li>
                            <li><a href="#">Бренды</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <div class="footer__title">Контакты</div>
                        <ul>
                            <li><a href="#">тел: +7 727 232 6265,</a></li>
                            <li><a href="#">тел: +7 727 232 6295.</a></li>
                            <li><a href="#">e-mail: info@menstyle.kz</a></li>
                        </ul>
                        <ul class="social__list row align-items-center">
                            <li>
                                <a href="#" class="social__link">
                                    <i class="icon fb__icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="social__link">
                                    <i class="icon inst__icon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="f__container row justify-content-between">
                    <div class="copyright">© 2020 Men Style</div>
                    <a href="#" class="created">Created by SlonWorks</a>
                </div>
            </div>
        </div>
    </footer>

    <script src="./js/jquery-3.2.1.min.js"></script>
    <script src="./js/jquery.migrate.3.0.0.js"></script>
    <script src="./js/maskedInputPlugin.min.js"></script>
    <!-- slider -->
    <script src="./libs/OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script src="./libs/slick/slick.min.js"></script>
    <!-- fancybox -->
    <script src="./libs/fancybox/jquery.fancybox.min.js"></script>
    <!-- wow aos -->
    <script src="./libs/wow/wow.min.js"></script>
    <script src="./libs/aos/aos.js"></script>
    <!-- jquery ui -->
    <script src="./libs/jquery-ui/jquery-ui.js"></script>
    <!-- validate -->
    <script src="./libs/validate/jquery.validate.js"></script>
     <!-- touch -->
     <script src="./libs/jquery.ui.touch-punch.min.js"></script>
     <!-- zoom -->
     <script src="./libs/zoom/jquery.zoom.js"></script>
    <!-- yandex map -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <!-- custom -->
    <script src="./js/custom.js"></script>
</body>
</html>