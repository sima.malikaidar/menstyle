<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./libs/OwlCarousel2-2.3.4/owl.carousel.css">
    <link rel="stylesheet" href="./libs/OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" href="./libs/slick/slick.css">
    <link rel="stylesheet" href="./libs/slick/slick-theme.css">
    <link rel="stylesheet" href="./libs/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="./libs/animate/animate.css">
    <link rel="stylesheet" href="./libs/aos/aos.css">
    <link rel="stylesheet" href="./libs/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="./css/style.css">
    <title>Men Style</title>
</head>
<body class="loading">
    <header class="header__absolute h-inner">
        <div class="b-container">
            <div class="row justify-content-between menu__desktop">
                <ul class="col-5 col-md-4 navbar row align-items-center">
                    <li>
                        <a href="#" class="navbar__link">
                            <i class="icon address__icon"></i>
                        </a>
                    </li>
                    <li class="nav__item submenu">
                        <a href="#" class="navbar__link">Одежда</a>
                        <div class="dropdown-wrapper nav__dropdown">
                            <div class="row">
                                <div class="col-7 dropdown__menu_left">
                                    <div class="dropdown-menu__container">
                                        <div class="row">
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Брюки</a></li>
                                                <li><a href="#">Деним</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                                <li><a href="#">Куртки</a></li>
                                                <li><a href="#">Нижнее белье</a></li>
                                                <li><a href="#">Верхняя одежда</a></li>
                                            </ul>
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Рубашки</a></li>
                                                <li><a href="#">Майки</a></li>
                                                <li><a href="#">Пиджаки</a></li>
                                                <li><a href="#">Трикотаж</a></li>
                                                <li><a href="#">Шорты</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                            </ul>
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Брюки</a></li>
                                                <li><a href="#">Деним</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                                <li><a href="#">Куртки</a></li>
                                                <li><a href="#">Нижнее белье</a></li>
                                                <li><a href="#">Верхняя одежда</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5 menu__banner">
                                    <div class="menu__banner-img" style="background-image: url(./img/menu-img.jpg);">
                                    </div>
                                    <div class="menu__banner-text">
                                        <p>до 50% на коллекцию BUGATTI</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav__item submenu">
                        <a href="#" class="navbar__link">Обувь</a>
                        <div class="dropdown-wrapper nav__dropdown">
                            <div class="row">
                                <div class="col-7 dropdown__menu_left">
                                    <div class="dropdown-menu__container">
                                        <div class="row">
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Брюки</a></li>
                                                <li><a href="#">Деним</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                                <li><a href="#">Куртки</a></li>
                                                <li><a href="#">Нижнее белье</a></li>
                                                <li><a href="#">Верхняя одежда</a></li>
                                            </ul>
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Рубашки</a></li>
                                                <li><a href="#">Майки</a></li>
                                                <li><a href="#">Пиджаки</a></li>
                                                <li><a href="#">Трикотаж</a></li>
                                                <li><a href="#">Шорты</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                            </ul>
                                            <ul class="col-4 submenu__list">
                                                <li><a href="#">Брюки</a></li>
                                                <li><a href="#">Деним</a></li>
                                                <li><a href="#">Костюмы</a></li>
                                                <li><a href="#">Куртки</a></li>
                                                <li><a href="#">Нижнее белье</a></li>
                                                <li><a href="#">Верхняя одежда</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5 menu__banner">
                                    <div class="menu__banner-img" style="background-image: url(./img/menu-img.jpg);">
                                    </div>
                                    <div class="menu__banner-text">
                                        <p>до 50% на коллекцию BUGATTI</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav__item">
                        <a href="#" class="navbar__link">Аксессуары</a>
                    </li>
                </ul>
                <div class="nav__logo">
                    <a href="#">
                        <img src="./img/logo.png">
                    </a>
                </div>
                <div class="col-5 col-md-4 justify-content-end">
                    <ul class="navbar row align-items-center justify-content-between">
                        <li class="nav__item">
                            <a href="#" class="navbar__link">Бренды</a>
                        </li>
                        <li>
                            <a href="#" class="navbar__link sale__link">
                                <span class="sale__circle">Sale</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="navbar__link search__link">
                                <i class="icon search__icon"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="navbar__link">
                                <i class="icon basket__icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                
            </div>
            <div class="row justify-content-between align-items-center menu__mobile">
                <a href="#" class="nav__btn-mobile">
                    <span></span>
                    <span></span>
                </a>
                <div class="nav__logo">
                    <a href="#">
                        <img src="./img/logo.png">
                    </a>
                </div>
                <a href="#" class="navbar__link mobile__icon">
                    <i class="icon basket__icon"></i>
                </a>
                <div class="burger__menu_mobile">
                    <div class="container">
                        <div class="navbar__mobile row justify-content-between align-items-center">
                            <div class="close__btn"></div>
                            <div class="nav__logo">
                                <a href="#">
                                    <img src="./img/logo.png">
                                </a>
                            </div>
                            <a href="#" class="navbar__link mobile__icon">
                                <i class="icon search__icon"></i>
                            </a>
                        </div>
                        <div class="nav__list-mob row flex-column align-items-center">
                            <ul>
                                <li>
                                    <a href="#" class="navbar__link sale__link _red">
                                        <span class="sale__circle">Sale</span>
                                    </a>
                                </li>
                                <li class="nav__item submenu active">
                                    <a href="#" class="navbar__link">Одежда</a>
                                    <div class="nav-mob__dropdown">
                                        <ul>
                                            <li><a href="#">Вся одежда</a></li>
                                            <li><a href="#">Рубашки</a></li>
                                            <li><a href="#">Пальто и куртки</a></li>
                                            <li><a href="#">Пуховики</a></li>
                                            <li><a href="#">Жилеты</a></li>
                                            <li><a href="#">Костюмы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav__item submenu">
                                    <a href="#" class="navbar__link">Обувь</a>
                                    <div class="nav-mob__dropdown">
                                        <ul>
                                            <li><a href="#">Вся одежда</a></li>
                                            <li><a href="#">Рубашки</a></li>
                                            <li><a href="#">Пальто и куртки</a></li>
                                            <li><a href="#">Пуховики</a></li>
                                            <li><a href="#">Жилеты</a></li>
                                            <li><a href="#">Костюмы</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="submenu">
                                    <a href="#" class="navbar__link">Аксессуары</a>
                                </li>
                                <li class="submenu">
                                    <a href="#" class="navbar__link">Бренды</a>
                                </li>
                                <li class="submenu">
                                    <a href="#" class="social__link">
                                        <i class="icon address__icon"></i>
                                        <span>Контакты</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Написать нам</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="col-md-3 social__list row align-items-center justify-content-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-wrapper search__dropdown">
            <div class="b-container">
                <div class="search__form row justify-content-between align-items-center">
                    <a href="#" class="navbar__link search__link">
                        <i class="icon search__icon _gray"></i>
                    </a>
                    <div class="input-search">
                        <input type="search" class="input" placeholder="Поиск...">
                        <button class="btn close__btn _gray"></button>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
