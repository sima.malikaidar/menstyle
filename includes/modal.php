<div id="modal--feedback" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__feedback">
        <div class="close__btn"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 modal__left">
                    <div class="modal__title">Оставьте заявку и мы свяжемся с вами! </div>
                    <p>Подпишитесь на рассылку, чтобы быть в курсе акций 
                        и специальных предложений, а также получить доступ 
                        к Закрытой Распродаже.</p>
                    <ul class="social__list row justify-content-between align-items-end">
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon phone__icon"></i>
                                <span class="social__name">+7 727 232 6265</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="social__link">
                                <i class="icon pen__icon"></i>
                                <span class="social__name">Menstyle@gmail.ru</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 modal__right">
                    <form class="form-group">
                        <div class="input-text">
							<select name="user__address" class="input select-ui">
								<option selected disabled hidden>Тема обращения</option>
								<option value="1">Нур-Султан</option>
								<option value="2">Алматы</option>
								<option value="3">Актобе</option>
							</select>
                        </div>
                        <div class="input-text">
                            <input type="text" class="input" placeholder="Имя*" name="user_name">
                        </div>
                        <div class="input-text">
                            <input type="text" class="input input_tel" placeholder="Номер телефона*" name="user_phone">
                        </div>
                        <div class="input-text">
                            <input type="email" class="input" placeholder="E-mail*" name="user_email">
                        </div>
                        <div class="input-text">
                            <textarea name="user_text" class="textarea" cols="10" placeholder="Сообщение*"></textarea>
                        </div>
                        <button type="submit" class="btn__circle">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal--success" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__success">
        <div class="close__btn"></div>
        <div class="container">
            <div class="modal__title">Cпасибо за подписку!</div>
            <p>Два раза в неделю мы будем присылать вам наши лучшие предложения и делиться только самыми интересными 
           и полезными новостями.</p>
        </div>
    </div>
</div>
<div id="modal--cart" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__cart">
        <div class="close__btn"></div>
        <div class="container">
            <div class="modal__title">Ваш товар успешно добавлен в корзину!</div>
            <a href="#" class="btn btn__fill__black">Продолжить покупки</a>
            <a href="#" class="btn btn__border__black">Перейти в корзину</a>
        </div>
    </div>
</div>
<div id="modal--size" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__size">
        <div class="close__btn"></div>
        <div class="size__content">
			<h2 class="remodal-title">Таблица размеров</h2>
			<div class="table-wrapper">
				<table class="size__table size__table_main">
					<tbody>
					<tr>
						<td><b>Мужская одежда</b></td>
						<td class="open-modal-click" data-modal="#modal--size-1"><a href="#">Носки</a></td>
						<td class="open-modal-click" data-modal="#modal--size-2"><a href="#">Нижнее белье</a></td>
						<td class="open-modal-click" data-modal="#modal--size-3"><a href="#">Обувь</a></td>
						<td class="open-modal-click" data-modal="#modal--size-4"><a href="#">Одежда</a></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="modal--size-1" class="modal__area">
    <div class="modal__block modal__size">
        <div class="close__btn"></div>
		<div class="size__content">
			<h2 class="remodal-title">Таблицы размеров мужских носков</h2>
			<h3 class="size-subtitle tt-up">Как определить свой размер для чулочно-носочных изделий</h3>
			<p>При подборе носков вы можете ориентироваться на размер своей обуви, на длину стопы или стельки. Если вы не знаете страну-производителя свой обуви и испытываете сложности с определением ее размера, то определите размер стопы или стельки в сантиметрах. Для этого воспользуйтесь измерительной лентой.</p>
				<br><br>
			<div class="size__images row">
				<div class="size__image">
					<img class="lazy" src="./img/size-1.png" style="">
					<p>Обведите стопу</p>
			</div>
			<div class="size__image">
				<img class="lazy" src="./img/size-2.png" style="">
				<p>Измерьте линейкой</p>
			</div>
				<br><br>
			<h3 class="size-subtitle tt-up">Как замерить указанные параметры для чулочно-носочных изделий</h3>
			<div class="size__description">
				<p class="title">Для стопы</p>
				<p class="content">— встаньте на лист бумаги одной ногой и обведите ее контур. Вам нужно измерить расстояние на полученной фигуре между большим пальцем и пяткой.</p>
			</div>
			<div class="size__description">
				<p class="title">Для стельки</p>
				<p class="content">— вытащите стельку из обуви, которую носите. Замерьте лентой расстояние между большим пальцем и пяткой.</p>
			</div>
				<br><br>
				<p>Воспользуйтесь размерной таблицей для мужчин, чтобы определить размер носков. Для расшифровки пользуйтесь любым удобным параметром: размером обуви, стопы или длиной стельки.</p>
				<br/><br/>
				<h3 class="size-subtitle">Мужские носки и гетры</h3>
				<div class="table-wrapper">
					<table class="size__table">
						<thead>
							<tr>
								<th>Россия</th>
								<th>Междунар. <br>INT</th>
								<th>Европа <br>EU</th>
								<th>Германия <br>GER</th>
								<th>Франция <br>FR</th>
								<th>США <br>USA</th>
								<th>Размер <br>обуви</th>
								<th>Стопа, <br>мм</th>
								<th>Стелька, <br>мм</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>23</td>
								<td>S</td>
								<td>37/38</td>
								<td>37/38</td>
								<td>37/38</td>
								<td>8</td>
								<td>36-37</td>
								<td>219-233</td>
								<td>224-238</td>
							</tr>
							<tr>
								<td>25</td>
								<td>M</td>
								<td>39/40</td>
								<td>39/40</td>
								<td>39/40</td>
								<td>9</td>
								<td>40-41</td>
								<td>246-260</td>
								<td>252-267</td>
							</tr>
							<tr>
								<td>25</td>
								<td>S</td>
								<td>39/40</td>
								<td>39/40</td>
								<td>39/40</td>
								<td>9</td>
								<td>38-39</td>
								<td>233-246</td>
								<td>238-252</td>
							</tr>
							<tr>
								<td>27</td>
								<td>M</td>
								<td>41/42</td>
								<td>41/42</td>
								<td>41/42</td>
								<td>10</td>
								<td>42-43</td>
								<td>260-273</td>
								<td>267-280</td>
							</tr>
							<tr>
								<td>29</td>
								<td>L</td>
								<td>43/44</td>
								<td>43/44</td>
								<td>43/44</td>
								<td>11</td>
								<td>44-45</td>
								<td>273-280</td>
								<td>280-296</td>
							</tr>
							<tr>
								<td>31</td>
								<td>XXL</td>
								<td>45/46</td>
								<td>45/46</td>
								<td>45/46</td>
								<td>12</td>
								<td>46-47</td>
								<td>280-306</td>
								<td>296-315</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="modal--size-2" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__size">
        <div class="close__btn"></div>
		<div class="size__content">
    		<h2 class="remodal-title">Таблицы размеров мужского нижнего белья</h2>
    		<p>
				Как определить свой размер нижнего белья <br><br>
				Для расшифровки размерной таблицы вам необходимо вычислить два значения: обхват талии и обхват бедер. Воспользуйтесь размерной лентой и запишите полученные результаты в сантиметрах.
			</p><br>
			<div class="size__images row">
				<div class="size__image">
					<img class="lazy" src="./img/size-3.png" style="">
					<p>Обхват талии</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-4.png" style="">
					<p>Обхват бедер</p>
				</div>
			</div>
			<p>Как замерить указанные параметры для нижнего белья</p>
			<br>
			<div class="size__description">
				<p class="title">Обхват талии</p>
				<p class="content">— не втягивая живот, проведите сантиметровую ленту по линии талии, а после — по спине. Сведите концы и посмотрите полученное значение.</p>
			</div>
			<div class="size__description">
				<p class="title">Обхват бедер</p>
				<p class="content">приложите ленту к самим высоким точкам на ягодицах.</p>
			</div>
			<br><br>
			<p>Найдите усредненное значение для полученных параметров в измерительной таблице поясного белья для мужчин — это и есть ваш размер. Ориентируйтесь на страну-производителя, чтобы выбрать соответствующий размер в представленной таблице.</p>
			<br><br>
			<h3 class="size-subtitle">Мужские трусы, плавки и другое нижнее белье</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<th>Россия</th>
							<th>Междунар. <br>INT</th>
							<th>Европа <br>EU</th>
							<th>Германия <br>GER</th>
							<th>Франция <br>FR</th>
							<th>Италия <br>IT</th>
							<th>Англия <br>UK</th>
							<th>США <br>USA</th>
							<th>Талия, <br>см</th>
							<th>Бедра, <br>см</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>44</td>
							<td>S</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>42</td>
							<td>36</td>
							<td>36</td>
							<td>78</td>
							<td>95.6</td>
						</tr>
						<tr>
							<td>46</td>
							<td>S</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>44</td>
							<td>38</td>
							<td>38</td>
							<td>82</td>
							<td>98.6</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>46</td>
							<td>40</td>
							<td>40</td>
							<td>84</td>
							<td>101.6</td>
						</tr>
						<tr>
							<td>50</td>
							<td>M</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>48</td>
							<td>42</td>
							<td>42</td>
							<td>88</td>
							<td>104.6</td>
						</tr>
						<tr>
							<td>52</td>
							<td>L</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>50</td>
							<td>42</td>
							<td>42</td>
							<td>92</td>
							<td>107.6</td>
						</tr>
						<tr>
							<td>54</td>
							<td>L</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>52</td>
							<td>44</td>
							<td>44</td>
							<td>96</td>
							<td>110.6</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>54</td>
							<td>46</td>
							<td>46</td>
							<td>100</td>
							<td>113.6</td>
						</tr>
						<tr>
							<td>58</td>
							<td>XL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>56</td>
							<td>48</td>
							<td>48</td>
							<td>104</td>
							<td>116.6</td>
						</tr>
						<tr>
							<td>60</td>
							<td>XXL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>58</td>
							<td>50</td>
							<td>50</td>
							<td>108</td>
							<td>119.6</td>
						</tr>
						<tr>
							<td>62</td>
							<td>XXL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>60</td>
							<td>52</td>
							<td>52</td>
							<td>112</td>
							<td>122.6</td>
						</tr>
						<tr>
							<td>64</td>
							<td>XXXL</td>
							<td>58</td>
							<td>58</td>
							<td>58</td>
							<td>62</td>
							<td>54</td>
							<td>54</td>
							<td>116</td>
							<td>125.6</td>
						</tr>
						<tr>
							<td>66</td>
							<td>XXXL</td>
							<td>60</td>
							<td>60</td>
							<td>60</td>
							<td>64</td>
							<td>56</td>
							<td>56</td>
							<td>120</td>
							<td>128.6</td>
						</tr>
						<tr>
							<td>68</td>
							<td>XXXL</td>
							<td>62</td>
							<td>62</td>
							<td>62</td>
							<td>66</td>
							<td>58</td>
							<td>58</td>
							<td>124</td>
							<td>131.6</td>
						</tr>
						<tr>
							<td>70</td>
							<td>XXXL</td>
							<td>64</td>
							<td>64</td>
							<td>64</td>
							<td>68</td>
							<td>60</td>
							<td>60</td>
							<td>128</td>
							<td>134.6</td>
						</tr>
					</tbody>
				</table>
			</div>
  		</div>
  	</div>
</div>

<div id="modal--size-3" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__size">
        <div class="close__btn"></div>
		<div class="size__content">
    		<h2 class="remodal-title">Таблицы размеров мужской обуви</h2>
    		<p>Чтобы правильно выбрать размер обуви для мужчин, необходимо замерить длину стопы. Проще всего это сделать, замерив стельку той обуви, которую вы носите. Если нет возможности достать стельку, то замерьте длину стопы с помощью измерительной ленты.</p>
   		 	<br><br>
    		<h3 class="size-subtitle tt-up">Как правильно замерить указанные параметры для обуви</h3>
			<div class="size__images row">
				<div class="size__image">
					<img class="lazy" src="./img/size-5.png" style="">
					<p>Обведите стопу</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-6.png" style="">
					<p>Измерьте линейкой</p>
				</div>
			</div>
			<ul>
				<li>Опустите ноги на лист бумаги и обведите их по фигуре.</li>
				<li>Приложите измерительную ленту к пятке и проведите ее к большому пальцу.</li>
				<li>Запишите полученное значение в сантиметрах.</li>
			</ul>
			<br><br>
			<p>Для поиска размера в таблице используйте параметры той ноги, которая оказалась чуть больше. Расшифровка очень простая: сверьте длину своей стопы по размерной таблице и найдите нужное значение.</p>
			<br><br>
			<h3 class="size-subtitle">Мужские туфли, сапоги, ботинки и тд.</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
					<tr>
						<th>Россия</th>
						<th>Европа <br>EU</th>
						<th>Германия <br>GER</th>
						<th>Франция <br>FR</th>
						<th>Англия <br>UK</th>
						<th>США <br>USA</th>
						<th>Корея, <br>мм</th>
						<th>Бразилия <br>BR</th>
						<th>Стелька, <br>см</th>
						<th>Стопа, <br>см</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>38</td>
						<td>39</td>
						<td>39</td>
						<td>39</td>
						<td>5</td>
						<td>6</td>
						<td>246</td>
						<td>37</td>
						<td>24,5</td>
						<td>24,4</td>
					</tr>
					<tr>
						<td>38.5</td>
						<td>39.5</td>
						<td>39.5</td>
						<td>39.5</td>
						<td>5.5</td>
						<td>6.5</td>
						<td>249</td>
						<td>37.5</td>
						<td>24,7</td>
						<td>24,6</td>
					</tr>
					<tr>
						<td>39</td>
						<td>40</td>
						<td>40</td>
						<td>40</td>
						<td>6</td>
						<td>7</td>
						<td>252</td>
						<td>38</td>
						<td>25</td>
						<td>24,9</td>
					</tr>
					<tr>
						<td>40</td>
						<td>41</td>
						<td>41</td>
						<td>41</td>
						<td>7</td>
						<td>8</td>
						<td>258</td>
						<td>39</td>
						<td>25,5</td>
						<td>25,4</td>
					</tr>
					<tr>
						<td>40.5</td>
						<td>41.5</td>
						<td>41.5</td>
						<td>41.5</td>
						<td>7.5</td>
						<td>8.5</td>
						<td>264</td>
						<td>39.5</td>
						<td>26</td>
						<td>25,9</td>
					</tr>
					<tr>
						<td>41</td>
						<td>42</td>
						<td>42</td>
						<td>42</td>
						<td>8</td>
						<td>9</td>
						<td>271</td>
						<td>40</td>
						<td>26,5</td>
						<td>26,4</td>
					</tr>
					<tr>
						<td>42</td>
						<td>43</td>
						<td>43</td>
						<td>43</td>
						<td>9</td>
						<td>10</td>
						<td>279</td>
						<td>41</td>
						<td>27</td>
						<td>26,9</td>
					</tr>
					<tr>
						<td>43</td>
						<td>44</td>
						<td>44</td>
						<td>44</td>
						<td>10</td>
						<td>11</td>
						<td>287</td>
						<td>42</td>
						<td>27,5</td>
						<td>27,4</td>
					</tr>
					<tr>
						<td>43.5</td>
						<td>44.5</td>
						<td>44.5</td>
						<td>44.5</td>
						<td>10.5</td>
						<td>11.5</td>
						<td>291</td>
						<td>42.5</td>
						<td>28</td>
						<td>27,9</td>
					</tr>
					<tr>
						<td>44</td>
						<td>45</td>
						<td>45</td>
						<td>45</td>
						<td>11</td>
						<td>12</td>
						<td>295</td>
						<td>43</td>
						<td>28,5</td>
						<td>28,4</td>
					</tr>
					<tr>
						<td>45</td>
						<td>46</td>
						<td>46</td>
						<td>46</td>
						<td>12</td>
						<td>13</td>
						<td>303</td>
						<td>44</td>
						<td>29</td>
						<td>28,9</td>
					</tr>
					<tr>
						<td>45.5</td>
						<td>46.5</td>
						<td>46.5</td>
						<td>46.5</td>
						<td>12.5</td>
						<td>13.5</td>
						<td>307</td>
						<td>44.5</td>
						<td>29,2</td>
						<td>29,1</td>
					</tr>
					<tr>
						<td>46</td>
						<td>47</td>
						<td>47</td>
						<td>47</td>
						<td>13</td>
						<td>14</td>
						<td>311</td>
						<td>45</td>
						<td>29,5</td>
						<td>29,4</td>
					</tr>
					<tr>
						<td>46.5</td>
						<td>47.5</td>
						<td>47.5</td>
						<td>47.5</td>
						<td>13.5</td>
						<td>14.5</td>
						<td>315</td>
						<td>45.5</td>
						<td>30</td>
						<td>29,9</td>
					</tr>
					<tr>
						<td>47</td>
						<td>31,9</td>
						<td>46</td>
						<td>305</td>
						<td>304</td>
						<td>48</td>
						<td>48</td>
						<td>48</td>
						<td>14</td>
						<td>15</td>
					</tr>
					</tbody>
				</table>
			</div>
  		</div>
  	</div>
</div>

<div id="modal--size-4" class="modal__area">
    <div class="modal__layer"></div>
    <div class="modal__block modal__size">
        <div class="close__btn"></div>
		<div class="size__content">
			<h2 class="remodal-title">Таблицы размеров мужской одежды</h2>
			<p>Как определить свой типовой размер?</p>
			<br><br>
			<div class="size__images row">
				<div class="size__image">
					<img class="lazy" src="./img/size-7.png" style="">
					<p>Обхват груди</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-8.png" style="">
					<p>Обхват талии</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-9.png" style="">
					<p>Обхват бедер</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-10.png" style="">
					<p>Длина рукава</p>
				</div>
				<div class="size__image">
					<img class="lazy" src="./img/size-11.png" style="">
					<p>Длина внутреннего <br>шва брючин</p>
				</div>
			</div>
			<ul>
				<li>Для рубашек, пиджаков, джемперов, верхней одежды основной параметр — обхват груди.</li>
				<li>Для брюк необходимые параметры — обхват талии и бёдер.</li>
			</ul>
			<br><br>
			<p>Как измерить свои параметры</p>
			<br><br>
			<div class="size__description">
				<p class="title">Обхват груди</p>
				<p class="content">— сантиметровая лента проходит по наиболее выступающим точкам груди, далее — под подмышечными впадинами, на спине — немного выше.</p>
			</div>
			<div class="size__description">
				<p class="title">Обхват талии</p>
				<p class="content">— измеряется строго по линии талии. На любой фигуре это область в районе пупка.</p>
			</div>
			<div class="size__description">
				<p class="title">Обхват бедер</p>
				<p class="content">— сантиметровая лента проходит по наиболее выступающим точкам ягодиц.</p>
			</div>
			<br><br>
			<p>Как мы измеряем параметры вещей</p>
			<br><br>
			<div class="size__description">
				<p class="title">Обхват груди</p>
				<p class="content">— расстояние на уровне груди под проймами рукавов.</p>
			</div>
			<div class="size__description">
				<p class="title">Обхват талии</p>
				<p class="content">— расстояние на уровне верхнего среза в поясных изделиях для мужчин.</p>
			</div>
			<div class="size__description">
				<p class="title">Обхват бедер</p>
				<p class="content">— расстояние на уровне выступающих точек ягодиц.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина спереди</p>
				<p class="content">— измеряется спереди от самой высокой точки плеча через грудь до низа изделия.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина по спинке</p>
				<p class="content">— измеряется от седьмого шейного позвонка вдоль линии середины спины до низа изделия.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина рукава</p>
				<p class="content">— измеряется по внешней стороне рукава от его вершины до низа.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина рукава от горловины</p>
				<p class="content">— измеряется от основания горловины или воротника до низа рукава.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина внутреннего шва брючин</p>
				<p class="content">— измеряется по внутреннему шву брючин от промежности до низа.</p>
			</div>
			<div class="size__description">
				<p class="title">Длина шорт</p>
				<p class="content">— измеряется в сантиметрах от верхнего шва до низа изделия.</p>
			</div>
			<br><br>
			<p>Для вашего удобства расшифровка размерной таблицы легкой и верхней одежды дана в размерах разных стран-производителей.</p>
			<br><br>
			<h3 class="size-subtitle">Мужские cорочки, рубашки</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<th>Россия</th>
							<th>Междунар. <br>INT</th>
							<th>Европа <br>EU</th>
							<th>Германия <br>GER</th>
							<th>Франция <br>FR</th>
							<th>Италия <br>IT</th>
							<th>Англия <br>UK</th>
							<th>Грудь, <br>см</th>
							<th>Шея, <br>см</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>42</td>
							<td>XS</td>
							<td>36</td>
							<td>36</td>
							<td>36</td>
							<td>36</td>
							<td>14</td>
							<td>81-86</td>
							<td>36</td>
						</tr>
						<tr>
							<td>44</td>
							<td>S</td>
							<td>37</td>
							<td>37</td>
							<td>37</td>
							<td>37</td>
							<td>14.5</td>
							<td>86-91</td>
							<td>37</td>
						</tr>
						<tr>
							<td>44-46</td>
							<td>S</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>15</td>
							<td>91-96</td>
							<td>38</td>
						</tr>
						<tr>
							<td>46</td>
							<td>M</td>
							<td>39</td>
							<td>39</td>
							<td>39</td>
							<td>39</td>
							<td>15.5</td>
							<td>96-101</td>
							<td>39</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>15.75</td>
							<td>96-101</td>
							<td>40</td>
						</tr>
						<tr>
							<td>48-50</td>
							<td>L</td>
							<td>41</td>
							<td>41</td>
							<td>41</td>
							<td>41</td>
							<td>16</td>
							<td>101-106</td>
							<td>41</td>
						</tr>
						<tr>
							<td>50</td>
							<td>L</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>16.5</td>
							<td>101-106</td>
							<td>42</td>
						</tr>
						<tr>
							<td>50-52</td>
							<td>XL</td>
							<td>43</td>
							<td>43</td>
							<td>43</td>
							<td>43</td>
							<td>17</td>
							<td>106-111</td>
							<td>43</td>
						</tr>
						<tr>
							<td>52</td>
							<td>XL</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>17.5</td>
							<td>106-111</td>
							<td>44</td>
						</tr>
						<tr>
							<td>52-54</td>
							<td>XXL</td>
							<td>45</td>
							<td>45</td>
							<td>45</td>
							<td>45</td>
							<td>17.5</td>
							<td>111-116</td>
							<td>45</td>
						</tr>
						<tr>
							<td>54</td>
							<td>XXL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>18</td>
							<td>111-116</td>
							<td>46</td>
						</tr>
						<tr>
							<td>54-56</td>
							<td>XXXL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>18.5</td>
							<td>118-122</td>
							<td>47</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XXXL</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>19</td>
							<td>118-122</td>
							<td>48</td>
						</tr>
						<tr>
							<td>56-58</td>
							<td>4XL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>19.5</td>
							<td>124-130</td>
							<td>49</td>
						</tr>
						<tr>
							<td>58</td>
							<td>4XL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>20</td>
							<td>132-140</td>
							<td>50</td>
						</tr>
						<tr>
							<td>58-60</td>
							<td>5XL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>20.5</td>
							<td>142-146</td>
							<td>51</td>
						</tr>
						<tr>
							<td>60</td>
							<td>5XL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>21</td>
							<td>148-154</td>
							<td>52</td>
						</tr>
						<tr>
							<td>62</td>
							<td>6XL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>21.5</td>
							<td>156-162</td>
							<td>54</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h3 class="size-subtitle">Мужские брюки, джинсы, шорты</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
					<tr>
						<th>Россия</th>
						<th>Европа <br>EU</th>
						<th>Германия <br>GER</th>
						<th>Франция <br>FR</th>
						<th>Италия <br>IT</th>
						<th>Англия <br>UK</th>
						<th>США <br>USA</th>
						<th>Джинсы <br>США</th>
						<th>Талия, <br>см</th>
						<th>Бедра, <br>см</th>
						<th>Талия, <br>дюймы</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>44</td>
						<td>38</td>
						<td>38</td>
						<td>38</td>
						<td>42</td>
						<td>34</td>
						<td>2-4</td>
						<td>29</td>
						<td>73-78</td>
						<td>94-97</td>
						<td>29-30</td>
					</tr>
					<tr>
						<td>46</td>
						<td>40</td>
						<td>40</td>
						<td>40</td>
						<td>44</td>
						<td>36</td>
						<td>4-6</td>
						<td>30</td>
						<td>79-82</td>
						<td>98-101</td>
						<td>31-32</td>
					</tr>
					<tr>
						<td>48</td>
						<td>42</td>
						<td>42</td>
						<td>42</td>
						<td>46</td>
						<td>38</td>
						<td>6-8</td>
						<td>31</td>
						<td>83-86</td>
						<td>102-105</td>
						<td>33-34</td>
					</tr>
					<tr>
						<td>50</td>
						<td>44</td>
						<td>44</td>
						<td>44</td>
						<td>48</td>
						<td>40</td>
						<td>8-10</td>
						<td>32</td>
						<td>87-90</td>
						<td>106-108</td>
						<td>35-36</td>
					</tr>
					<tr>
						<td>52</td>
						<td>46</td>
						<td>46</td>
						<td>46</td>
						<td>50</td>
						<td>42</td>
						<td>10-12</td>
						<td>33</td>
						<td>91-94</td>
						<td>109-112</td>
						<td>38-40</td>
					</tr>
					<tr>
						<td>54</td>
						<td>48</td>
						<td>48</td>
						<td>48</td>
						<td>52</td>
						<td>44</td>
						<td>12-14</td>
						<td>34</td>
						<td>95-98</td>
						<td>112-116</td>
						<td>40-41</td>
					</tr>
					<tr>
						<td>56</td>
						<td>50</td>
						<td>50</td>
						<td>50</td>
						<td>54</td>
						<td>46</td>
						<td>16</td>
						<td>35</td>
						<td>99-102</td>
						<td>116-120</td>
						<td>42-44</td>
					</tr>
					<tr>
						<td>58</td>
						<td>52</td>
						<td>52</td>
						<td>52</td>
						<td>56</td>
						<td>48</td>
						<td>16-18</td>
						<td>36</td>
						<td>103-106</td>
						<td>121-124</td>
						<td>45-46</td>
					</tr>
					<tr>
						<td>60</td>
						<td>54</td>
						<td>54</td>
						<td>54</td>
						<td>58</td>
						<td>50</td>
						<td>20</td>
						<td>38</td>
						<td>107-110</td>
						<td>124-128</td>
						<td>47-48</td>
					</tr>
					<tr>
						<td>62</td>
						<td>56</td>
						<td>56</td>
						<td>56</td>
						<td>60</td>
						<td>52</td>
						<td>22</td>
						<td>40</td>
						<td>111-114</td>
						<td>129-133</td>
						<td>48-49</td>
					</tr>
					<tr>
						<td>64</td>
						<td>58</td>
						<td>58</td>
						<td>58</td>
						<td>62</td>
						<td>54</td>
						<td>24</td>
						<td>42</td>
						<td>115-118</td>
						<td>129-133</td>
						<td>50-51</td>
					</tr>
					<tr>
						<td>66</td>
						<td>60</td>
						<td>60</td>
						<td>60</td>
						<td>64</td>
						<td>56</td>
						<td>26</td>
						<td>44</td>
						<td>119-122</td>
						<td>133-135</td>
						<td>52-53</td>
					</tr>
					<tr>
						<td>68</td>
						<td>62</td>
						<td>62</td>
						<td>62</td>
						<td>66</td>
						<td>58</td>
						<td>28</td>
						<td>46</td>
						<td>123-126</td>
						<td>135-138</td>
						<td>54-55</td>
					</tr>
					</tbody>
				</table>
			</div>
			<h3 class="size-subtitle">Мужские худи, свитера, кардиганы</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<td>Россия</td>
							<td>Междунар. <br>INT</td>
							<td>Европа <br>EU</td>
							<td>Германия <br>GER</td>
							<td>Франция <br>FR</td>
							<td>Италия <br>IT</td>
							<td>Англия <br>UK</td>
							<td>США <br>US</td>
							<td>Грудь, <br>см</td>
							<td>Талия, <br>см</td>
							<td>Рукав, <br>см</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>44</td>
							<td>XS</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>42</td>
							<td>34</td>
							<td>34</td>
							<td>86-89</td>
							<td>73-78</td>
							<td>59-60</td>
						</tr>
						<tr>
							<td>46</td>
							<td>S</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>44</td>
							<td>36</td>
							<td>36</td>
							<td>90-93</td>
							<td>79-82</td>
							<td>60-61</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>46</td>
							<td>38</td>
							<td>38</td>
							<td>94-97</td>
							<td>83-86</td>
							<td>61-63</td>
						</tr>
						<tr>
							<td>50</td>
							<td>L</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>48</td>
							<td>40</td>
							<td>40</td>
							<td>98-101</td>
							<td>87-90</td>
							<td>62-63</td>
						</tr>
						<tr>
							<td>52</td>
							<td>XL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>50</td>
							<td>42</td>
							<td>42</td>
							<td>102-105</td>
							<td>91-94</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>54</td>
							<td>XXL</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>52</td>
							<td>44</td>
							<td>44</td>
							<td>106-109</td>
							<td>95-98</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XXXL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>54</td>
							<td>46</td>
							<td>46</td>
							<td>110-113</td>
							<td>99-102</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>58</td>
							<td>XXXL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>56</td>
							<td>48</td>
							<td>48</td>
							<td>114-117</td>
							<td>103-106</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>60</td>
							<td>XXXL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>58</td>
							<td>50</td>
							<td>50</td>
							<td>118-121</td>
							<td>107-110</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>62</td>
							<td>4XL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>60</td>
							<td>52</td>
							<td>52</td>
							<td>122-125</td>
							<td>111-114</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>64</td>
							<td>4XL</td>
							<td>58</td>
							<td>58</td>
							<td>58</td>
							<td>62</td>
							<td>54</td>
							<td>54</td>
							<td>126-129</td>
							<td>115-118</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>66</td>
							<td>5XL</td>
							<td>60</td>
							<td>60</td>
							<td>60</td>
							<td>64</td>
							<td>56</td>
							<td>60</td>
							<td>130-133</td>
							<td>119-122</td>
							<td>65-69</td>
						</tr>
						<tr>
							<td>68</td>
							<td>5XL</td>
							<td>62</td>
							<td>62</td>
							<td>62</td>
							<td>66</td>
							<td>58</td>
							<td>62</td>
							<td>134-137</td>
							<td>123-126</td>
							<td>65-69</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h3 class="size-subtitle">Мужские футболки, поло, майки</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<th>Россия</th>
							<th>Междунар. <br>INT</th>
							<th>Европа <br>EU</th>
							<th>Германия <br>GER</th>
							<th>Франция <br>FR</th>
							<th>Италия <br>IT</th>
							<th>Англия <br>UK</th>
							<th>США <br>US</th>
							<th>Грудь, <br>см</th>
							<th>Талия, <br>см</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>44</td>
							<td>XS</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>42</td>
							<td>34</td>
							<td>34</td>
							<td>86-89</td>
							<td>73-78</td>
						</tr>
						<tr>
							<td>46</td>
							<td>S</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>44</td>
							<td>36</td>
							<td>36</td>
							<td>90-93</td>
							<td>79-82</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>46</td>
							<td>38</td>
							<td>38</td>
							<td>94-97</td>
							<td>83-86</td>
						</tr>
						<tr>
							<td>50</td>
							<td>L</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>48</td>
							<td>40</td>
							<td>40</td>
							<td>98-101</td>
							<td>87-90</td>
						</tr>
						<tr>
							<td>52</td>
							<td>XL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>50</td>
							<td>42</td>
							<td>42</td>
							<td>102-105</td>
							<td>91-94</td>
						</tr>
						<tr>
							<td>54</td>
							<td>XXL</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>52</td>
							<td>44</td>
							<td>44</td>
							<td>106-109</td>
							<td>95-98</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XXXL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>54</td>
							<td>46</td>
							<td>46</td>
							<td>110-113</td>
							<td>99-102</td>
						</tr>
						<tr>
							<td>58</td>
							<td>XXXL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>56</td>
							<td>48</td>
							<td>48</td>
							<td>114-117</td>
							<td>103-106</td>
						</tr>
						<tr>
							<td>60</td>
							<td>XXXL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>58</td>
							<td>50</td>
							<td>50</td>
							<td>118-121</td>
							<td>107-110</td>
						</tr>
						<tr>
							<td>62</td>
							<td>4XL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>60</td>
							<td>52</td>
							<td>52</td>
							<td>122-125</td>
							<td>111-114</td>
						</tr>
						<tr>
							<td>64</td>
							<td>4XL</td>
							<td>58</td>
							<td>58</td>
							<td>58</td>
							<td>62</td>
							<td>54</td>
							<td>54</td>
							<td>126-129</td>
							<td>115-118</td>
						</tr>
						<tr>
							<td>66</td>
							<td>5XL</td>
							<td>60</td>
							<td>60</td>
							<td>60</td>
							<td>64</td>
							<td>56</td>
							<td>60</td>
							<td>130-133</td>
							<td>119-122</td>
						</tr>
						<tr>
							<td>68</td>
							<td>5XL</td>
							<td>62</td>
							<td>62</td>
							<td>62</td>
							<td>66</td>
							<td>58</td>
							<td>62</td>
							<td>134-137</td>
							<td>123-126</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h3 class="size-subtitle">Мужские пиджаки, блейзеры</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<th>Россия</th>
							<th>Междунар. <br>INT</th>
							<th>Европа <br>EU</th>
							<th>Германия <br>GER</th>
							<th>Франция <br>FR</th>
							<th>Италия <br>IT</th>
							<th>Англия <br>UK</th>
							<th>США <br>US</th>
							<th>Грудь, <br>см</th>
							<th>Талия, <br>см</th>
							<th>Рукав, <br>см</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>44</td>
							<td>XS</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>42</td>
							<td>34</td>
							<td>34</td>
							<td>90-93</td>
							<td>73-78</td>
							<td>59-60</td>
						</tr>
						<tr>
							<td>46</td>
							<td>S</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>44</td>
							<td>36</td>
							<td>36</td>
							<td>94-97</td>
							<td>79-82</td>
							<td>60-61</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>46</td>
							<td>38</td>
							<td>38</td>
							<td>98-101</td>
							<td>83-86</td>
							<td>61-63</td>
						</tr>
						<tr>
							<td>50</td>
							<td>L</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>48</td>
							<td>40</td>
							<td>40</td>
							<td>102-105</td>
							<td>87-90</td>
							<td>62-63</td>
						</tr>
						<tr>
							<td>52</td>
							<td>XL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>50</td>
							<td>42</td>
							<td>42</td>
							<td>106-109</td>
							<td>91-94</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>54</td>
							<td>XXL</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>52</td>
							<td>44</td>
							<td>44</td>
							<td>110-113</td>
							<td>95-98</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XXXL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>54</td>
							<td>46</td>
							<td>46</td>
							<td>114-117</td>
							<td>99-102</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>58</td>
							<td>XXXL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>56</td>
							<td>48</td>
							<td>48</td>
							<td>118-121</td>
							<td>103-106</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>60</td>
							<td>XXXL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>58</td>
							<td>50</td>
							<td>50</td>
							<td>122-125</td>
							<td>107-110</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>62</td>
							<td>4XL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>60</td>
							<td>52</td>
							<td>52</td>
							<td>126-129</td>
							<td>111-114</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>64</td>
							<td>4XL</td>
							<td>58</td>
							<td>58</td>
							<td>58</td>
							<td>62</td>
							<td>54</td>
							<td>54</td>
							<td>130-133</td>
							<td>115-118</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>66</td>
							<td>5XL</td>
							<td>60</td>
							<td>60</td>
							<td>60</td>
							<td>64</td>
							<td>56</td>
							<td>60</td>
							<td>134-137</td>
							<td>119-122</td>
							<td>65-69</td>
						</tr>
						<tr>
							<td>68</td>
							<td>5XL</td>
							<td>62</td>
							<td>62</td>
							<td>62</td>
							<td>66</td>
							<td>58</td>
							<td>62</td>
							<td>138-140</td>
							<td>123-126</td>
							<td>65-69</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h3 class="size-subtitle">Мужские куртки, пуховики, пальто, плащи</h3>
			<div class="table-wrapper">
				<table class="size__table">
					<thead>
						<tr>
							<th>Россия</th>
							<th>Междунар. <br>INT</th>
							<th>Европа <br>EU</th>
							<th>Германия <br>GER</th>
							<th>Франция <br>FR</th>
							<th>Италия <br>IT</th>
							<th>Англия <br>UK</th>
							<th>США <br>USA</th>
							<th>Грудь, <br>см</th>
							<th>Талия, <br>см</th>
							<th>Рукав, <br>см</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>44</td>
							<td>XS</td>
							<td>38</td>
							<td>38</td>
							<td>38</td>
							<td>42</td>
							<td>34</td>
							<td>34</td>
							<td>86-89</td>
							<td>73-78</td>
							<td>59-60</td>
						</tr>
						<tr>
							<td>46</td>
							<td>S</td>
							<td>40</td>
							<td>40</td>
							<td>40</td>
							<td>44</td>
							<td>36</td>
							<td>36</td>
							<td>90-93</td>
							<td>79-82</td>
							<td>60-61</td>
						</tr>
						<tr>
							<td>48</td>
							<td>M</td>
							<td>42</td>
							<td>42</td>
							<td>42</td>
							<td>46</td>
							<td>38</td>
							<td>38</td>
							<td>94-97</td>
							<td>83-86</td>
							<td>61-63</td>
						</tr>
						<tr>
							<td>50</td>
							<td>L</td>
							<td>44</td>
							<td>44</td>
							<td>44</td>
							<td>48</td>
							<td>40</td>
							<td>40</td>
							<td>98-101</td>
							<td>87-90</td>
							<td>62-63</td>
						</tr>
						<tr>
							<td>52</td>
							<td>XL</td>
							<td>46</td>
							<td>46</td>
							<td>46</td>
							<td>50</td>
							<td>42</td>
							<td>42</td>
							<td>102-105</td>
							<td>91-94</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>54</td>
							<td>XXL</td>
							<td>48</td>
							<td>48</td>
							<td>48</td>
							<td>52</td>
							<td>44</td>
							<td>44</td>
							<td>106-109</td>
							<td>95-98</td>
							<td>64-65</td>
						</tr>
						<tr>
							<td>56</td>
							<td>XXXL</td>
							<td>50</td>
							<td>50</td>
							<td>50</td>
							<td>54</td>
							<td>46</td>
							<td>46</td>
							<td>110-113</td>
							<td>99-102</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>58</td>
							<td>XXXL</td>
							<td>52</td>
							<td>52</td>
							<td>52</td>
							<td>56</td>
							<td>48</td>
							<td>48</td>
							<td>114-117</td>
							<td>103-106</td>
							<td>64-67</td>
						</tr>
						<tr>
							<td>60</td>
							<td>XXXL</td>
							<td>54</td>
							<td>54</td>
							<td>54</td>
							<td>58</td>
							<td>50</td>
							<td>50</td>
							<td>118-121</td>
							<td>107-110</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>62</td>
							<td>4XL</td>
							<td>56</td>
							<td>56</td>
							<td>56</td>
							<td>60</td>
							<td>52</td>
							<td>52</td>
							<td>122-125</td>
							<td>111-114</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>64</td>
							<td>4XL</td>
							<td>58</td>
							<td>58</td>
							<td>58</td>
							<td>62</td>
							<td>54</td>
							<td>54</td>
							<td>126-129</td>
							<td>115-118</td>
							<td>65-68</td>
						</tr>
						<tr>
							<td>66</td>
							<td>5XL</td>
							<td>60</td>
							<td>60</td>
							<td>60</td>
							<td>64</td>
							<td>56</td>
							<td>60</td>
							<td>130-133</td>
							<td>119-122</td>
							<td>65-69</td>
						</tr>
						<tr>
							<td>68</td>
							<td>5XL</td>
							<td>62</td>
							<td>62</td>
							<td>62</td>
							<td>66</td>
							<td>58</td>
							<td>62</td>
							<td>134-137</td>
							<td>123-126</td>
							<td>65-69</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
  	</div>
</div>
