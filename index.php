<? include './includes/header.php' ?>
<div class="main">
    <section class="main__slider__area">
        <div class="main__slider owl-carousel owl-loaded">
            <div class="slide-item" style="background-color: #e8c687;">
                <a href="#" class="cat__link">
                    <div class="slider-img mid__img">
                        <img src="./img/slide-1.jpg">
                        <div class="go__catalog-mob"></div>
                    </div>
                    <div class="slider__text">
                        <div class="sale-text">Sale</div>
                        <span class="brand__name">Bugatti  BARCELONA</span>
                        <div class="sale-percent">Скидка-20%</div>
                        <div class="promocode">По промокоду Menstyle20</div>
                    </div>              
                </a>
                <div class="info__bar">
                    <div class="b-container">
                        <div class="row justify-content-between align-items-end">
                            <ul class="col-md-3 social__list row align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                        <span class="social__name">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                        <span class="social__name">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="col-md-3 info__center">
                                <p>2020</p>
                                <p>Bugatti COAT - Classic coat - navy</p>
                            </div>
                            <ul class="col-md-3 social__list row justify-content-end align-items-center">
                                <li>
                                    <a href="#" class="social__link open-modal-click" data-modal="#modal--feedback">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Написать нам</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item" style="background-color: #797979;">
                <a href="#" class="cat__link">
                    <div class="slider-img mid__img">
                        <img src="./img/slide-5.png">
                        <div class="go__catalog-mob"></div>
                    </div>
                    <div class="slider__text">
                        <div class="sale-text">Sale</div>
                        <span class="brand__name">Deniel hechter</span>
                        <div class="sale-percent">Скидка-20%</div>
                        <div class="promocode">По промокоду Menstyle20</div>
                    </div>
                </a>
                <div class="info__bar">
                    <div class="b-container">
                        <div class="row justify-content-between align-items-end">
                            <ul class="col-md-3 social__list row align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                        <span class="social__name">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                        <span class="social__name">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="col-md-3 info__center">
                                <p>2020</p>
                                <p>Bugatti COAT - Classic coat - navy</p>
                            </div>
                            <ul class="col-md-3 social__list row justify-content-end align-items-center">
                                <li>
                                    <a href="#" class="social__link open-modal-click" data-modal="#modal--feedback">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Написать нам</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item" style="background-color: #9b5452;">
                <a href="#" class="cat__link">
                    <div class="slider-img mid__img">
                        <img src="./img/slide-3.png">
                        <div class="go__catalog-mob"></div>
                    </div>
                    <div class="slider__text">
                        <div class="sale-text">Sale</div>
                        <span class="brand__name">KARL LAGERFeld</span>
                        <div class="sale-percent">Скидка-20%</div>
                        <div class="promocode">По промокоду Menstyle20</div>
                    </div>
                </a>
                <div class="info__bar">
                    <div class="b-container">
                        <div class="row justify-content-between align-items-end">
                            <ul class="col-md-3 social__list row align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                        <span class="social__name">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                        <span class="social__name">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="col-md-3 info__center">
                                <p>2020</p>
                                <p>Bugatti COAT - Classic coat - navy</p>
                            </div>
                            <ul class="col-md-3 social__list row justify-content-end align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Написать нам</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item" style="background-color: #a9b1a6;">
                <a href="#" class="cat__link">
                    <div class="slider-img mid__img">
                        <img src="./img/slide-5.png">
                        <div class="go__catalog-mob"></div>
                    </div>
                    <div class="slider__text">
                        <div class="sale-text">Sale</div>
                        <span class="brand__name">JACQUES BRITT</span>
                        <div class="sale-percent">Скидка-20%</div>
                        <div class="promocode">По промокоду Menstyle20</div>
                    </div>
                </a>
                <div class="info__bar">
                    <div class="b-container">
                        <div class="row justify-content-between align-items-end">
                            <ul class="col-md-3 social__list row align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon fb__icon"></i>
                                        <span class="social__name">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon inst__icon"></i>
                                        <span class="social__name">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="col-md-3 info__center">
                                <p>2020</p>
                                <p>Bugatti COAT - Classic coat - navy</p>
                            </div>
                            <ul class="col-md-3 social__list row justify-content-end align-items-center">
                                <li>
                                    <a href="#" class="social__link">
                                        <i class="icon pen__icon"></i>
                                        <span class="social__name">Написать нам</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="controll__counter js-tour-counter">
            <b>03</b> / <span class="js-tour-size">09</span>
        </div>
    </section>
    <section class="new__collection">
        <div class="new__collect-slider row">
            <div class="new__collect-for col-md-6">
                <a href="#" class="slick-item">
                    <img src="./img/new-1.jpg">
                    <div class="new__collect-for--text">
                        <p>Bugatti BARCELONA</p>
                        <p>2020</p>
                    </div>
                </a>
                <a href="#" class="slick-item">
                    <img src="./img/new-1.jpg">
                    <div class="new__collect-for--text">
                        <p>Bugatti BARCELONA</p>
                        <p>2020</p>
                    </div>
                </a>
                <a href="#" class="slick-item">
                    <img src="./img/new-1.jpg">
                    <div class="new__collect-for--text">
                        <p>Bugatti BARCELONA</p>
                        <p>2020</p>
                    </div>
                </a>
            </div>
            <div class="col-md-6 new__collect-right">
                <div class="new__collect-nav">
                    <div class="slick-item">
                        <a href="#" class="collect__product">
                            <span class="collect__title">Bugatti Barcelona <br> 2020 collection</span>
                            <div class="collect__img">
                                <img src="./img/new-thumb-1.jpg">
                            </div>
                            <div class="collect__bottom">
                                <div class="collect__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="collect__price">75.000 <span>т</span></div>
                            </div>
                        </a>
                    </div>
                    <div class="slick-item">
                        <a href="#" class="collect__product">
                            <span class="collect__title">Bugatti Barcelona <br> 2020 collection</span>
                            <div class="collect__img">
                                <img src="./img/new-thumb-1.jpg">
                            </div>
                            <div class="collect__bottom">
                                <div class="collect__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="collect__price">75.000 <span>т</span></div>
                            </div>
                        </a>
                    </div>
                    <div class="slick-item">
                        <a href="#" class="collect__product">
                            <span class="collect__title">Bugatti Barcelona <br> 2020 collection</span>
                            <div class="collect__img">
                                <img src="./img/new-thumb-1.jpg">
                            </div>
                            <div class="collect__bottom">
                                <div class="collect__name">Bugatti COAT - Classic coat - navy</div>
                                <div class="collect__price">75.000 <span>т</span></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="banners banner1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 banner__item">
                    <a href="#" class="banner__product" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <div class="banner__product-img">
                            <img src="./img/banner-1.png">
                        </div>
                        <div class="banner__product-name">
                            <span>Bugatti COAT - Classic coat - navy</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 banner__item">
                    <a href="#" class="banner__product" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                        <div class="banner__product-img">
                            <img src="./img/banner-2.png">
                        </div>
                        <div class="banner__product-name">
                            <span>Bugatti COAT - Classic coat - navy</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="categories">
        <div class="container">
            <span class="page__title">Категории</span>
            <div class="categories__links _desktop">
                <ul class="categories__list row justify-content-center">
                    <li><a href="#">Брюки <span>(20)</span></a></li>
                    <li><a href="#">Рубашки <span>(120)</span></a></li>
                    <li><a href="#">Аксессуары <span>(60)</span></a></li>
                    <li><a href="#">Галстуки <span>(60)</span></a></li>
                    <li><a href="#">Костюмы <span>(60)</span></a></li>
                    <li><a href="#">Пиджаки <span>(60)</span></a></li>
                </ul>
                <ul class="categories__list _second row justify-content-center">
                    <li><a href="#">Новинки <span>(20)</span></a></li>
                    <li><a href="#">Футболки<span>(120)</span></a></li>
                    <li><a href="#">Джинсы <span>(60)</span></a></li>
                    <li><a href="#">Жилетки <span>(60)</span></a></li>
                    <li><a href="#">Все <span>(1000)</span></a></li>
                </ul>
            </div>
            <div class="categories__links _mobile">
                <ul class="categories__list owl-carousel">
                    <li><a href="#">Брюки <span>(20)</span></a></li>
                    <li><a href="#">Рубашки <span>(120)</span></a></li>
                    <li><a href="#">Аксессуары <span>(60)</span></a></li>
                    <li><a href="#">Галстуки <span>(60)</span></a></li>
                    <li><a href="#">Костюмы <span>(60)</span></a></li>
                    <li><a href="#">Пиджаки <span>(60)</span></a></li>
                    <li><a href="#">Новинки <span>(20)</span></a></li>
                    <li><a href="#">Футболки<span>(120)</span></a></li>
                    <li><a href="#">Джинсы <span>(60)</span></a></li>
                    <li><a href="#">Жилетки <span>(60)</span></a></li>
                    <li><a href="#">Все <span>(1000)</span></a></li>
                </ul>
            </div>
            <div class="product__slider _desktop owl-carousel">
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-1.jpg">
                        <div class="sticker">
                            <span>NEW</span>
                        </div>
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">55.000 <span class="valute">₸</span> <span class="old__price">75.000 ₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-3.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-4.jpeg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
            </div>
            <div class="product__slider _mobile justify-content-between">
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-1.jpg">
                        <div class="sticker">
                            <span>NEW</span>
                        </div>
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">55.000 <span class="valute">₸</span> <span class="old__price">75.000 ₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-3.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-4.jpeg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
                <a href="#" class="product__block">
                    <div class="product__img">
                        <img src="./img/product-2.jpg">
                        <div class="product__hover">Купить</div>
                    </div>
                    <div class="product__bottom">
                        <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                        <div class="product__price">75.000 <span class="valute">₸</span></div>
                    </div>
                </a>
            </div>
            <a href="#" class="go-to__cat">Перейти в каталог</a>
        </div>
    </section>
    <section class="best__collection banner2">
        <div class="banner-content">
            <div class="container">
                <a href="#" class="banner__top" data-aos="fade-up" data-aos-duration="1000">
                    <img src="./img/banner-3.jpg">
                </a>
                <div class="banner__title">
                    <div class="banner__text">Лучшее из нового</div>
                    <p>Новые коллекции Bottega Veneta, Burberry, Bugatti, JOOP</p>
                </div>
            </div>
        </div>
    </section>
    <section class="brands _desktop">
        <div class="container">
            <span class="page__title">Бренды</span>
            <div class="brands__content">
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-5.png">Baumler</a></li>
                    <li><a href="#" data-img="./img/slide-1.jpg">Benvenuto</a></li>
                    <li><a href="#" data-img="./img/banner-6.png">Blend</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Bottega</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-1.png">Brax</a></li>
                    <li><a href="#" data-img="./img/banner-2.png">Bugatti </a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Casa Moda</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Casual Friday</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-1.png">Commander</a></li>
                    <li><a href="#" data-img="./img/banner-6.png">Daniel Hechter</a></li>
                    <li class="active"><a href="#"data-img="./img/banner-5.png">Desoto</a></li>
                    <li><a href="#" data-img="./img/banner-1.png">Fraas</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/slide-1.jpg">Jacques</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Britt</a></li>
                    <li class="nonactive"><a href="#">JOOP!</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Karl Lagerfeld</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Maerz Marvelis</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-1.png">Olymp</a></li>
                    <li><a href="#" data-img="./img/banner-6.png">Ragman</a></li>
                    <li><a href="#" data-img="./img/banner-2.png">Rene Lezard</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Roeckl</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Roy Robson</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-5.png">Seidensticker</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Strellson</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Venti</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Vitacci</a></li>
                </ul>
                <ul class="brands__list row justify-content-center">
                    <li><a href="#" data-img="./img/banner-5.png">Wegener</a></li>
                    <li><a href="#" data-img="./img/banner-5.png">Windsor</a></li>
                </ul>
                <div id="tooltip"></div>
            </div>
            <a href="#" class="all__brands">Все</a>
        </div>
    </section>
    <section class="banner3">
        <div class="banner-top" style="background-image: url('./img/banner-4.jpg')" data-aos="fade-in" data-aos-duration="1500">
        </div>
        <div class="banner-content">
            <div class="container">
                <div class="row">
                    <div class="banner__left">
                        <div class="banner__left1" data-aos="fade-up" data-aos-duration="2000">
                            <img src="./img/banner-6.png">
                        </div>
                        <div class="banner__left2" data-aos="fade-up" data-aos-duration="2000">
                            <img src="./img/slide-1.jpg">
                        </div>
                    </div>
                    <div class="banner__center">
                        <div class="banner__text">
                            <p>Новые образы для нового года. Костюмы из коллекции Benvenuto Winter 2020</p>
                        </div>
                        <a href="#" class="go-to__cat">Перейти в каталог</a>
                    </div>
                    <div class="banner__right row justify-content-end" data-aos="fade-up" data-aos-duration="2000">
                        <img src="./img/banner-5.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="info__seo">
        <div class="container">
            <div class="row">
                <div class="col-4 info__seo_left">
                    <div class="seo__slog">Men Style – один из лучших мультибрендовых магазинов мужской одежды и обуви в Казахстане </div>
                    <a href="#">Оплата и возврат</a>
                    <a href="#">Доставка</a>
                </div>
                <div class="col-8">
                    <ul class="tab-menu seo-menu row">
                        <li class="active"><a href="#seo-tab1">Одежда</a></li>
                        <li><a href="#seo-tab2">Обувь</a></li>
                        <li><a href="#seo-tab3">Аксессуары</a></li>
                        <li><a href="#seo-tab4">Бренды</a></li>
                        <li><a href="#seo-tab5">Бренды</a></li>
                    </ul>
                    <div class="tab-item active" data-id="seo-tab1">
                        <div class="row info__seo_right">
                            <ul>
                                <li class="active"><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Рубашки</a></li>
                                <li><a href="#">Майки</a></li>
                                <li><a href="#">Пиджаки</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><a href="#">Шорты</a></li>
                                <li><a href="#">Костюмы</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="seo-tab2">
                        <div class="row info__seo_right justify-content-end">
                            <ul>
                                <li class="active"><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Рубашки</a></li>
                                <li><a href="#">Майки</a></li>
                                <li><a href="#">Пиджаки</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><a href="#">Шорты</a></li>
                                <li><a href="#">Костюмы</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="seo-tab3">
                        <div class="row info__seo_right justify-content-end">
                            <ul>
                                <li class="active"><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Рубашки</a></li>
                                <li><a href="#">Майки</a></li>
                                <li><a href="#">Пиджаки</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><a href="#">Шорты</a></li>
                                <li><a href="#">Костюмы</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="seo-tab4">
                        <div class="row info__seo_right justify-content-end">
                            <ul>
                                <li class="active"><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Рубашки</a></li>
                                <li><a href="#">Майки</a></li>
                                <li><a href="#">Пиджаки</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><a href="#">Шорты</a></li>
                                <li><a href="#">Костюмы</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-item" data-id="seo-tab5">
                        <div class="row info__seo_right justify-content-end">
                            <ul>
                                <li class="active"><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Рубашки</a></li>
                                <li><a href="#">Майки</a></li>
                                <li><a href="#">Пиджаки</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><a href="#">Шорты</a></li>
                                <li><a href="#">Костюмы</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Брюки</a></li>
                                <li><a href="#">Деним</a></li>
                                <li><a href="#">Костюмы</a></li>
                                <li><a href="#">Куртки</a></li>
                                <li><a href="#">Нижнее белье</a></li>
                                <li><a href="#">Верхняя одежда</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe">
        <div class="row">
            <div class="col-md-4 subscribe__left">
                <img src="./img/slide-2.jpg">
            </div>
            <div class="col-md-8 subscribe__right">
                <div class="subs__content">
                    <div class="subs__title">Не пропустите самое интересное! </div>
                    <div class="subs--text">
                        <p>Подпишитесь на , чтобы быть в курсе акций и специальных предложений, а также получить доступ к Закрытой Распродаже.</p>
                    </div>
                    <form class="form row">
                        <div class="s__input ">
                            <i class="icon email__icon"></i>
                            <input type="email" class="input s-input" placeholder="Введите свой E-mail">
                        </div>
                        <button type="submit" class="btn btn__arrow subs__btn open-modal-click" data-modal="#modal--success">Подписаться</button>
                        <button class="btn subs__btn-mob">
                            <i class="icon email__icon"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>
<? include './includes/modal.php' ?>
