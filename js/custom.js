function createIframe(key) {
	var iframe = $('<iframe class="video__media" src="https://www.youtube.com/embed/' + key + '?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; autoplay" allowfullscreen></iframe>');

	return iframe;
}

function createLink(key) {
	var link = $('<a href="javascript:;" tabindex="-1" class="video__link"><img src="https://i.ytimg.com/vi/'+key+'/maxresdefault.jpg"><button class="video__button" type="button" tabindex="-1" aria-label="Запустить видео"><svg width="29px" height="29px" viewBox="0 0 357 357" fill="#f45b69"><polygon class="video__button-shape" points="38.25,0 38.25,357 318.75,178.5" /></svg></button></a>');

	return link;
}

function destroyIframe() {
	var iframe = $('.video iframe'),
		item = iframe.parent(),
		key = item.data('key'),
		link = createLink(key);

	item.append(link);
	iframe.remove();
}

function playVideo() {
	$('.video').on('click', '.video__link', function (e) {
		e.preventDefault();
		var link = $(this),
			item = link.parent(),
			key = item.data('key'),
			iframe = createIframe(key);

		item.append(iframe);
		link.remove();
	});
}

function scrollDown(){
	$('.btn__down, .header__left .btn__help').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 1000, 'linear');
	});
}
function langDropdown(){
	$el_languages = $('.lang__dropdown');
    $el_languages.hover(function () {
		$el_languages.find('li').show();
		$el_languages.find('li').children('span').addClass('open');
    }, function () {
		$el_languages.find('li:not(.active)').hide();
		$el_languages.find('li').children('span').removeClass('open');
	});
}
function navDropDowns()
{
	$(".navbar > li.nav__item.submenu").hover(
		function ()
		{
			var ww = $(window).width();
			if (ww > 768) $(this).find(".dropdown-wrapper").stop().slideDown();
			$(this).parents('.header__absolute').addClass('active');
			$('.search__dropdown').fadeOut();
		},
		function ()
		{
			$(this).find(".dropdown-wrapper").stop().slideUp();
			$(this).parents('.header__absolute').removeClass('active');
		}
	);
	$('.nav__btn-mobile').on('click', function(){
		$('.burger__menu_mobile').addClass('open');
	});
	$('.navbar__mobile .close__btn').on('click', function(){
		$('.burger__menu_mobile').removeClass('open');
	});
	$('.nav__list-mob li.submenu>a').on('click', function(e){
		e.preventDefault();
		$(this).next('.nav-mob__dropdown').slideToggle();
		$(this).parent().toggleClass('active');
	})
}
function openSearch(){
	$(".navbar__link.search__link, .mobile__icon .search__icon").on('click',
		function ()
		{
			$('.header__absolute').toggleClass('active');
			var ww = $(window).width();
			$(".dropdown-wrapper.search__dropdown").stop().fadeToggle();
			$('body').addClass('pop');
		}
	);
	$('.input-search .close__btn').on('click', function(e){
		e.preventDefault();
		$(this).parents('.search__dropdown').fadeOut();
		$(this).parents('.header__absolute').removeClass('active');
	})
	$(document).mouseup(function (e) {
		var container = $(".search__dropdown");
		var block = $('.search__dropdown');
		if (container.has(e.target).length && $(e.target).closest('.ui-selectmenu-menu').length === 0 && $(e.target).closest('.search__dropdown').length === 0){    
			block.fadeOut();
			$('.header__absolute').removeClass('active');
			$('body').removeClass('pop');
		}
	});
}
function indexSlider() {
	var slider = $('.main__slider');
	sliderCounter = $('.js-tour-counter'),
	sliderSize = $('.js-tour-size');
	slider.owlCarousel({
		items: 1,
		stagePadding: 0,
		margin: 0,
		nav: true,
		dots: false,
		loop: true,
		mouseDrag: true,
		lazyLoad: true,
		animateOut: 'fadeOut',
    	animateIn: 'fadeIn',
		autoplay: false,
		onInitialized: function(event) {
			var size = event.item.count;
			if (size < 10)
			sliderSize.text('0'+event.item.count);
		},
		responsive : {
			0 : {
				dots: true,
				nav: true
			},
			480 : {
				dots: true,
				nav: true
			},
			768 : {
				dots: true,
				nav: true
			}
		}
	});
	function counter(event) {
		var items = event.item.count;     // Number of items
		var item = event.item.index + event.page.size;     // Position of the current item
		
		var tempFix = item - (event.relatedTarget.clones().length / 2);

		if (tempFix > items) {
			tempFix = tempFix - items;
		}

		console.log(tempFix);
		if (tempFix <= 0) {
			tempFix += items;
		}

		if (tempFix < 10) {
			tempFix = '0' + tempFix;
		}

		sliderCounter.find('b').html(tempFix);
	}
	slider.on('changed.owl.carousel', function (event) {
		counter(event);
	});
	slider.on('drag.owl.carousel', function(event) {
		document.ontouchmove = function (e) {
			e.preventDefault();
		}
	});
}
function productSlider(){
	var pslider = $('.product__slider ');
	if($(window).width() >= 768){
		pslider.owlCarousel({
			items: 4,
			slideBy: 4,
			stagePadding: 0,
			margin: 30,
			nav: true,
			dots: true,
			loop: true,
			mouseDrag: true,
			lazyLoad: true,
			autoplay: false,
			responsive: {
				1024: {
					slideBy: 4
				}
			}
		});
	}
	var cslider = $('.categories__list');
	if($(window).width() <= 768){
		cslider.owlCarousel({
			items: 3,
			stagePadding: 20,
			margin: 20,
			nav: false,
			dots: false,
			loop: true,
			mouseDrag: true,
			lazyLoad: true,
			autoplay: false,
			responsive : {
				0 : {
					items: 2,
					stagePadding: 50,
				},
				480 : {
					items: 2
				},
				768 : {
					items: 3,
				}
			}
		});
		
	}
	if($(window).width() <= 990){
		$('.city__tabs-menu').owlCarousel({
			items: 3,
			stagePadding: 20,
			margin: 20,
			nav: false,
			dots: false,
			loop: true,
			mouseDrag: true,
			lazyLoad: true,
			autoplay: false,
			responsive : {
				0 : {
					items: 2,
					stagePadding: 50,
				},
				480 : {
					items: 3,
				},
				768 : {
					items: 3,
				}
			}
		});
	}
}
function hoverBrand(){
	var toolTip = document.getElementById('tooltip');
	$('.brands__content').on('mousemove', function(e){
		mouseX = e.clientX + 1,  //  get x position
		mouseY = e.clientY - 200;  //  get y position
		toolTip.setAttribute('style', 'top:' + mouseY + 'px; left:' + mouseX + 'px;');
	});
	$('.brands__content').on('mouseover', showTt)
	function showTt(e){
		if (e.target.tagName === 'A'){  //  test if hovered element is link
			toolTip.setAttribute('class', 'show');  //  show the tooltip
			var ttImage = e.target.dataset.img;  //  get the data attribute of hovered a, for example banner5.jpg
			console.log(ttImage);
			toolTip.innerHTML = '<img src="' + ttImage + '">';   //  
		} else{
			toolTip.setAttribute('class', ''); 
		}
	}
}

function prettify(num) {
	var n = num.toString();
	return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ');
}

function rangePrice() {
	$(".range" ).each(function(idx, el) {
        var minLimit = $(el).data('min'),
            maxLimit = $(el).data('max');
        
        $(el).slider({
            range: true,
            min: minLimit,
            max: maxLimit,
            values: [ minLimit, maxLimit ],
            change: function( event, ui ) {
                $(el).parent().find('.min').val(Intl.NumberFormat().format(parseInt(ui.values[ 0 ])));
                $(el).parent().find('.max').val(Intl.NumberFormat().format(parseInt(ui.values[ 1 ])));
            },
            slide: function( event, ui ) {
                $(el).parent().find('.min').val(Intl.NumberFormat().format(parseInt(ui.values[ 0 ])));
                $(el).parent().find('.max').val(Intl.NumberFormat().format(parseInt(ui.values[ 1 ])));
            }
        });
        $(el).parent().find('.min').on('change', function() {
            var minVal = $(this).val().replace(/[^\d]+/g, '');
            
            if (minVal > maxLimit)
                minVal = maxLimit
            if (minVal < minLimit)
                minVal = minLimit

            $(this).val(minVal);
            
            if (minVal > $(el).slider( "values", 1 ))
                $(el).slider( "values", 1, minVal );
            
            $(el).slider( "values", 0, minVal );
        });
        $(el).parent().find('.max').on('change', function() {
            var maxVal = $(this).val().replace(/[^\d]+/g, '');

            if (maxVal > maxLimit)
                maxVal = maxLimit
            if (maxVal < minLimit)
                maxVal = minLimit
            
            $(this).val(maxVal);

            if (maxVal < $(el).slider( "values", 0 ))
                $(el).slider( "values", 0, maxVal );
                
            $(el).slider( "values", 1, $(this).val() );
		});
		$(el).parent().find('.min, .max').on('input', function() {
			var value = prettify($(this).val());
			$(this).val(value);
		})

		
		
	});
}
function openTabs(){
	$(".tab-menu li a").click(function(e) {  
        $("div [data-id]").removeClass("active");  
        $("div [data-id='" + $(this).attr("href").replace("#","") + "']").addClass("active");
		$(this).parent().addClass('active').siblings().removeClass('active');
		$(this).parent().addClass('active').parents('.owl-item').siblings().children('li').removeClass('active');
        e.preventDefault();
    });
}
function addMinusNumbers(){
	$('.p-number .minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		console.log(count);
        $input.val(count);
		$input.change();
		if($input.val() <= 1){
			$(this).addClass('disable');
		}
		else{
			$(this).removeClass('disable');
		}
        return false;
	});
    $('.p-number .plus').click(function () {
		var $input = $(this).parent().find('input');
		count = parseInt($input.val()) + 1;
		$input.val(count);
		console.log($input.val());
		$input.change();
		if($input.val() > 1){
			$(this).parent('.p-number').children('.minus').removeClass('disable');
		}
        return false;
    });
}
function validateForms(){
	$('.form-group').validate({
        rules: {
            user_phone: {
                required: true,
            },
            user_email: {
                required: true,
                email: true
            },
            user_name: {
                required: true
			},
			user_surname: {
                required: true
			},
			user_phone: {
                required: true
			},
			user_city: {
                required: true
			},
			user_address: {
                required: true
			},
			user_index: {
                required: true
            },
            user_check: {
                required: true,
                checked: true
            }
        },
        messages: {
            user_phone: {
                required: 'Обязательное поле'
            },
            user_email: {
                required: 'Обязательное поле',
                email: 'Пожалуйста, введите корректный адрес электронной почты'
            },
            user_name: {
                required: 'Обязательное поле'
			},
			user_surname: {
                required: 'Обязательное поле'
			},
			user_phone: {
                required: 'Обязательное поле'
			},
			user_city: {
                required: 'Обязательное поле'
			},
			user_address: {
                required: 'Обязательное поле'
			},
			user_index: {
                required: 'Обязательное поле'
            },
            user_check: {
                required: 'Обязательное поле'
            }
        },
        onfocusout: function(element) {
			if ($(element).valid() == true ) {
				$('.tel__text').addClass('show')
			}
			return $(element).valid();
		}
    })
}
function openModal(){
	$('.open-modal-click').on('click', function(e){
		e.preventDefault();
        var modal = $(this).attr('data-modal');
		$(modal).fadeIn();
        $('body').addClass('modal-open');
    });
    $('.modal__area .close__btn, .modal__layer').on('click', function(e){
        e.preventDefault();
		$(this).parents('.modal__area').fadeOut();
        $('body').removeClass('modal-open');
	});
	$(document).mouseup(function (e) {
		var container = $(".modal__area");
		var block = $('.modal__area');
		console.log(e.target);
		if (container.has(e.target).length === 0 && $('.ui-selectmenu-menu').has(e.target).length === 0){ 
			block.fadeOut();
			$('body').removeClass('modal-open');
		}
	});
}
function yandexMap(mapId, mapOptions) {
    var map;
    
    this.mapConstructor = function() {
        map = new ymaps.Map(mapId, mapOptions);
        map.behaviors.disable('scrollZoom');

        return map;
    }();
    
    this.addMarksToMap = function(marksList) {
        var currentPlacemarks = marksList.map(function (value) {
            return new ymaps.Placemark(value.center, {
                balloonContent: '<p>' + value.title + '<br>' + value.balloonContent + '</p>',
                hintContent: value.hintContent,
                id: value.id
            }, value.markOption);
        });

        return ymaps.geoQuery(currentPlacemarks).addToMap(map);
    }
    this.removeMark = function(proporties) {
        return ymaps.geoQuery(map.geoObjects).search(proporties).removeFromMap(map);
    }
    this.clearMap = function() {
        return map.geoObjects.removeAll();
    }
    this.destroyMap = function() {
        map.destroy();
        return null;
    }
    this.updateMapSize = function() {
        return map.container.fitToViewport();
    }
    this.setMapCenter = function(center) {
        return map.setCenter(center);
    }
    this.updateMarks = function(marksList) {
        this.clearMap();
        this.addMarksToMap(marksList);
	}
	this.setMapZoom = function(value) {
        return map.setZoom(value, {duration: 400});
    }
	return this;
}
jQuery(document).ready(function($){
	navDropDowns();
	openSearch();
	playVideo();
	scrollDown();
	langDropdown();
	rangePrice();
	openTabs();
	addMinusNumbers();
	validateForms();
	openModal();
	hoverBrand();
	$('[name="user__address"]').selectmenu({
		placeholder: 'Город'
	});
	// input mask
	$('.input_tel').inputmask({
		"mask":"+7(799)999-99-99",
		"clearMaskOnLostFocus": true,
		"showMaskOnHover": false,
		"clearIncomplete": true,
	});
	$(".input_range").inputmask({
		// "mask":"999 999", 
		"placeholder": ' ',
		"clearMaskOnLostFocus": true,
		"showMaskOnHover": false, 
		"clearIncomplete": true,
		// regex: "/(\d)(?=(\d{3})+$)/g, '$1 '"
	});
	// price = price.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
	indexSlider();
	productSlider();
	$('.new__collect-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		speed: 1500,
		cssEase: 'linear',
		asNavFor: '.new__collect-nav'
	  });
	$('.new__collect-nav').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.new__collect-for',
		fade: true,
		dots: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					dots: true
				}
			},
		]
	});
	$('.cat__detail-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		vertical: true,
		asNavFor: '.cat__detail-nav',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					dots: true
				}
			},
		]
	  });
	$('.cat__detail-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		asNavFor: '.cat__detail-for',
		fade: true,
		dots: false,
		focusOnSelect: true,
		vertical: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					dots: true
				}
			},
		]
	});
	$('.cat__detail-for').on('wheel', function(e){
		if (e.originalEvent.deltaY > 0) {
			$(this).slick('slickNext');
		} else {
			$(this).slick('slickPrev');
		}
	});
	$('.cat__detail-for').hover(function(){
		$('body').addClass('modal-open');
	}, function(){
		$('body').removeClass('modal-open');
	})
	$('.down__toggle').on('click', function(e){
        e.preventDefault();
        $(this).next().slideToggle();
        $(this).toggleClass('open');
	});
	$('.filter__open').on('click', function(e){
		e.preventDefault();
		$('.filter__sidebar').fadeIn();
	});
	$('.filter__header-mobile .close__btn').on('click', function(e){
		e.preventDefault();
		$('.filter__sidebar').fadeOut();
	});
	$('.listing__controls--sort .close__btn').on('click', function(e){
		e.preventDefault();
		$('.listing__controls--sort').fadeOut();
	});
	$('.btn__sort').on('click', function(e){
		e.preventDefault();
		$('.listing__controls--sort').fadeIn();
	})
	$('.cart__product-item .close__btn').on('click', function(e){
		e.preventDefault();
		$(this).parents('.cart__product-item').hide();
	});
	if($(window).width() <= 768){
		$('.footer__title').on('click', function(e){
			e.preventDefault();
			console.log('click')
			$(this).next('ul').slideToggle();
		});
	}
	$('.all__brands').on('click', function(e){
		e.preventDefault();
		$('.brands__content').addClass('open');
		$(this).hide();
	});
	if($(window).width() > 768){
		$('.jqueryZoom').zoom({});
	}
	$('.checking').on('click', function(e){
		e.preventDefault();
		var ele = $(this).find('input');
		if(ele.is(':checked')){
			ele.removeAttr('checked');
		}else{
			ele.attr('checked', 'checked');
		}
	});
	$(window).on('scroll', function(e){
		var topFilter = $(window).scrollTop();
		if(topFilter > 90){
			$('.listing__controls__mobile').addClass('fix');
		}
		else{
			$('.listing__controls__mobile').removeClass('fix');
		}
	});

	ymaps.ready(function () {
        if ($('#map').length > 0) {
            var mapOptions = {
                center: [48.136207, 67.153550],
                zoom: 5,
                controls: ['smallMapDefaultSet'],
            }
            var markOption = {
                hideIconOnBalloonOpen :false,
                iconImageHref: '../img/mark.png',
                iconImageSize: [33,33],
                balloonShadow: false,
                balloonPanelMaxMapArea: 0,
                iconLayout: 'default#image',
                openBalloonOnClick: false
            }
            
            var contacts = [
                {
                    id: 0,
                    center: [43.26832607452854, 76.93681450000001],
                    title: 'Алматы',
                    balloonContent: 'г. Ташкент, ул. Ойбека 38а',
                    hintContent: '',
                    markOption: markOption
				},
				{
					id: 1,
					center: [51.128207, 71.430411],
					title: 'Нур-Султан',
					balloonContent: 'Address some text',
					hintContent: 'Text on hover 2',
					markOption: markOption
				},
				{
					id: 2,
					center: [50.300371, 57.154555],
					title: 'Актобе',
					balloonContent: 'Address some text',
					hintContent: 'Text on hover 2',
					markOption: markOption
				},
				{
					id: 3,
					center: [49.807754, 73.088504],
					title: 'Караганда',
					balloonContent: 'Address some text',
					hintContent: 'Text on hover 2',
					markOption: markOption
				},
				{
					id: 4,
					center: [52.285577, 76.940947],
					title: 'Павлодар',
					balloonContent: 'Address some text',
					hintContent: 'Text on hover 2',
					markOption: markOption
				}
            ];
            var map = new yandexMap('map', mapOptions);
			map.addMarksToMap(contacts);

			$('.city__tabs-menu li a').on('click', function(){
				map.setMapCenter([+$(this).data('wid'), +$(this).data('len')]);
				map.setMapZoom(8);
			});
        }
	});
	AOS.init({
		duration: 1200,
	});
});

