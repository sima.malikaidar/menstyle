<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Шаблон</title>
    <style type="text/css">
        table{width: 100%}
    </style>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #a9b1a6; min-width: 340px; font-size: 1px; line-height: normal;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width=100% style="background: #a9b1a6; height: 25px;">
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle">
                <table cellpadding="0" cellspacing="0" border="0" width="600" class="table600" style="max-width: 600px; min-width: 320px; background: #ffffff;">
                    <tr>
                        <td valign="middle">
                            <table cellpadding="0" cellspacing="0" width=100% style="height: 75px;">
                                <tr>
                                    <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                    <td align="center">
                                        <a href="#" style="display: block; width: 117px; height: 40px;">
                                            <img src="./img/logo.png">
                                        </a>
                                    </td>
                                    <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style=" height: 50px;">
                                <tr>
                                    <td width="40" style="width: 40px; max-width: 40px; min-width: 40px;">&nbsp;</td>
                                    <td>
                                        <a href="#" target="_blank" style="font-family: 'Tahoma'; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none; font-weight: normal;">   
                                            <font face="Arial, sans-serif" color="#ababab" style="font-size: 13px; line-height: 16px; text-decoration: none;">
                                                <span style="font-family: 'Tahoma', sans-serif; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none;">Одежда</span>
                                            </font>
                                       </a>
                                    </td>
                                    <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                    <td>
                                        <a href="#" target="_blank" style="font-family: 'Tahoma'; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none; font-weight: normal;">   
                                            <font face="Arial, sans-serif" color="#ababab" style="font-size: 13px; line-height: 16px; text-decoration: none;">
                                                <span style="font-family: 'Tahoma', sans-serif; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none;">Обувь</span>
                                            </font>
                                       </a>
                                    </td>
                                    <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                    <td>
                                        <a href="#" target="_blank" style="font-family: 'Tahoma'; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none; font-weight: normal;">   
                                            <font face="Arial, sans-serif" color="#ababab" style="font-size: 13px; line-height: 16px; text-decoration: none;">
                                                <span style="font-family: 'Tahoma', sans-serif; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none;">Аксессуары</span>
                                            </font>
                                       </a>
                                    </td>
                                    <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                    <td>
                                        <a href="#" target="_blank" style="font-family: 'Tahoma'; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none; font-weight: normal;">   
                                            <font face="Arial, sans-serif" color="#ababab" style="font-size: 13px; line-height: 16px; text-decoration: none;">
                                                <span style="font-family: 'Tahoma', sans-serif; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none;">Бренды</span>
                                            </font>
                                       </a>
                                       <td width="45" style="width: 45px; max-width: 45px; min-width: 45px;">&nbsp;</td>
                                    </td>
                                    <td>
                                        <a href="#" target="_blank" style="font-family: 'Tahoma'; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none; font-weight: normal;">   
                                            <font face="Arial, sans-serif" color="#ababab" style="font-size: 13px; line-height: 16px; text-decoration: none;">
                                                <span style="font-family: 'Tahoma', sans-serif; color: #343434; font-size: 14px; line-height: 16px; text-decoration: none;"><img src="./img/sale-link.png"></span>
                                            </font>
                                       </a>
                                       <td width="40" style="width: 40px; max-width: 40px; min-width: 40px;">&nbsp;</td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 25px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 290px;">
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td>
                                        <img src="./img/mail-img-1.png" style="width: 100%; height: 290px;">
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 30px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="centers">
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <tr>
                                                <td valign="center">
                                                    <span style="font-family: 'Tahoma'; color: #000000; font-size: 24px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Лучшее из нового</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 20px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="centers">
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <tr>
                                                <td valign="center">
                                                    <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Новые коллекции Bottega Veneta, Burberry, Bugatti, JOOP</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 33px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table valign="center" cellpadding="0" cellspacing="0" border="0" width=100%>
                                            <tr>
                                               <td align="center" valign="center" height="40">
                                                  <a href="#" target="_blank" width="235" style="width: 235px !important; max-width: 160px; min-width: 160px; background: #282828; display: block; width: 100%; height: 40px; font-family: Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 13px; line-height: 40px; text-decoration: none; white-space: nowrap;">
                                                        <span style="font-family: 'Tahoma'; color: #ffffff; font-size: 13px; line-height: 40px; text-decoration: none; white-space: nowrap;">в&nbsp;магазин</span>
                                                  </a>
                                               </td>
                                            </tr>
                                         </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 48px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #ebebeb; height: 1px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 40px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="centers">
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <tr>
                                                <td valign="center">
                                                    <span style="font-family: 'Tahoma'; color: #343434; font-size: 14px; text-transform: uppercase; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Вам может понравиться</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 30px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-1.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="10" style="width: 10px; max-width: 10px; min-width: 10px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-2.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="10" style="width: 10px; max-width: 10px; min-width: 10px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-3.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 25px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-1.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="10" style="width: 10px; max-width: 10px; min-width: 10px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-2.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="10" style="width: 10px; max-width: 10px; min-width: 10px;">&nbsp;</td>
                                    <td style="height: 250px;">
                                        <img src="./img/product-3.jpg" style="max-width: 100%; max-height: 100%;">
                                        <span style="font-size: 11px; height: 13px; display: block;"></span>
                                        <span style="font-size: 11px; ">Bugatti COAT - Classic coat - navy</span>
                                        <span style="font-size: 11px; ">55.000 ₸</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 65px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="centers">
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <tr>
                                                <td valign="center">
                                                    <a href="#" style="font-family: 'Tahoma'; color: #343434; font-size: 10px; text-transform: uppercase; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">перейти в каталог</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 65px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #ebebeb; height: 1px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 65px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="centers">
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <tr>
                                                <td valign="center">
                                                    <span style="font-family: 'Tahoma'; color: #343434; font-size: 14px; text-transform: uppercase; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Категории</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 43px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100%>
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 25px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 25px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">.</span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                    <td valign="center">
                                        <span style="font-family: 'Tahoma'; color: #000000; font-size: 14px; line-height: 24px; display: block; font-weight: normal; display: block; text-align: center;">Брюки<span>(20)</span></span>
                                    </td>
                                    <td width="15" style="width: 15px; max-width: 15px; min-width: 15px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #fff; height: 100px;">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width=100% style="background: #282828; height: 98px;">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="height: 33px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100%>
                                            <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="height: 116px;">
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #fff;  font-size: 9px; font-weight: bold; text-transform: uppercase; display: block; font-weight: normal;  display: block;">Главная страница</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width=100% style="height: 22px;">
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #fff;  font-size: 9px; font-weight: bold; text-transform: uppercase; display: block; font-weight: normal;  display: block;">категории</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width=100% style="height: 22px;">
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #fff;  font-size: 9px; font-weight: bold; text-transform: uppercase; display: block; font-weight: normal;  display: block;">Информация</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width=100% style="height: 22px;">
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #fff;  font-size: 9px; font-weight: bold; text-transform: uppercase; display: block; font-weight: normal;  display: block;">Контакты</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="40" style="width: 79px; max-width: 79px; min-width: 79px;">&nbsp;</td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">Чтобы не пропустить наши новости и акции, добавьте </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;"><a href="#" style="text-decoration: underline;">confirmation@info.kz</a> в свою адресную книгу. Вы получилиэто </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">письмо, потому что подписаны на рассылку интернет-магазина</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">Если вы не хотите получать рассылку, перейдите по ссылке:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;"><a href="#" style="text-decoration: underline;">Отписаться от рассылки</a></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="height: 27px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                                <td>
                                                    <table width=100% cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr>
                                                            <td style="width: 10px; max-width: 10px; min-width: 10px; height: 12px; max-height: 12px; min-height: 12px;">
                                                                <img src="./img/svg/facebook-logo-white-2.svg" style="max-width: 100%; max-height: 100%">
                                                            </td>
                                                            <td width="50" style="width: 7px; max-width: 7px; min-width: 7px;">&nbsp;</td>
                                                            <td>
                                                                <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">Facebook </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="108" style="width: 110px; max-width: 110px; min-width: 110px;">&nbsp;</td>
                                                <td>
                                                    <table width=100% cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr>
                                                            <td style="width: 10px; max-width: 10px; min-width: 10px; height: 12px; max-height: 12px; min-height: 12px;">
                                                                <img src="./img/svg/instagram-white-2.svg" style="max-width: 100%; max-height: 100%">
                                                            </td>
                                                            <td width="50" style="width: 7px; max-width: 7px; min-width: 7px;">&nbsp;</td>
                                                            <td>
                                                                <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">Instagram  </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="135" style="width: 135px; max-width: 135px; min-width: 135px;">&nbsp;</td>
                                                <td>
                                                    <table width=100% cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr>
                                                            <td style="width: 10px; max-width: 10px; min-width: 10px; height: 12px; max-height: 12px; min-height: 12px;">
                                                                <img src="./img/phone-white.png" style="max-width: 100%; max-height: 100%">
                                                            </td>
                                                            <td width="50" style="width: 7px; max-width: 7px; min-width: 7px;">&nbsp;</td>
                                                            <td>
                                                                <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">+7 727 232 6265 </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="height: 27px;">
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                                <td>
                                                    <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">© Авторские права принадлежат компании Sauvage, 2019.</span>
                                                </td>
                                                <td width="30" style="width: 30px; max-width: 30px; min-width: 30px;">&nbsp;</td>
                                                <td>
                                                    <span style="font-family: 'Tahoma'; color: #f0ecec;  font-size: 11px; font-weight: normal; line-height: 24px; display: block; font-weight: normal;  display: block;">Номер регистрации 4521150</span>
                                                </td>
                                                <td width="50" style="width: 50px; max-width: 50px; min-width: 50px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width=100% style="height: 40px;">
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width=100% style="background: #a9b1a6; height: 25px;">
                </table>
            </td>
        </tr>
   </table>
</body>
</html>