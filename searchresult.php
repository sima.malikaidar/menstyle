<? include './includes/header2.php' ?>
<div class="main bg-gray p-90">
    <section class="search__results">
        <div class="search__content">
            <div class="b-container">
                <div class="search__form row justify-content-between align-items-center">
                    <a href="#" class="navbar__link search__link">
                        <i class="icon search__icon _gray"></i>
                    </a>
                    <div class="input-search">
                        <input type="search" class="input" placeholder="Поиск...">
                        <button class="btn close__btn _gray"></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="search__result-block">
            <span class="search__text">По запросу: “Bronx” найдено 12 совпадений</span>
        </div>
        <div class="search__result-content">
            <div class="container">
                <div class="row">  
                    <div class="col-md-12">               
                        <div class="product__list cat__products row">
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-1.jpg">
                                    <div class="sticker">
                                        <span>NEW</span>
                                    </div>
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-1.jpg">
                                    <div class="sticker">
                                        <span>NEW</span>
                                    </div>
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-2.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">55.000 <span class="valute">₸</span> <span class="old__price">75.000 ₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-3.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-4.jpeg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-2.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-2.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-2.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                            <a href="#" class="product__block">
                                <div class="product__img">
                                    <img src="./img/product-2.jpg">
                                    <div class="product__hover">Купить</div>
                                </div>
                                <div class="product__bottom">
                                    <div class="product__name">Bugatti COAT - Classic coat - navy</div>
                                    <div class="product__price">75.000 <span class="valute">₸</span></div>
                                </div>
                            </a>
                        </div>
                        <div class="pagination">
                            <a href="#" class="prev nonactive"><i class="icon arrow__icon"></i> <span>Prev</span> </a>
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li>-</li>
                                <li><a href="#">120</a></li>
                            </ul>
                            <a href="#" class="next"><span>Next</span> <i class="icon arrow__icon"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<? include './includes/footer.php' ?>